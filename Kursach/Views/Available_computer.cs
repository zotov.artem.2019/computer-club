namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Available computers")]
    public partial class Available_computer
    {
        [Key]
        [Column("Computer ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Computer_ID { get; set; }
    }
}
