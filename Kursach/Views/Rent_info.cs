namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Rent info")]
    public partial class Rent_info
    {
        [Key]
        [Column("Rent ID", Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Rent_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Client { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Employee { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Computer { get; set; }

        [Key]
        [Column("Start date and time", Order = 4, TypeName = "smalldatetime")]
        public DateTime Start_date_and_time { get; set; }

        [Key]
        [Column(Order = 5)]
        public TimeSpan Duration { get; set; }
    }
}
