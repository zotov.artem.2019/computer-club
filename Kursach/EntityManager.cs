﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach
{
    internal class EntityManager
    {
        ComputerClubModel db = new ComputerClubModel();
        public IEnumerable GetCards()
        {
            var cards = from card in db.Club_cards
                        select new
                        {
                            card.Card_ID,
                            card.Type,
                            card.Client,
                            card.Date_of_issue,
                            card.Validity
                        };
            return cards.ToList();
        }
      
        public IEnumerable GetAllEmployeesByPosition(byte position)
        {
            var employees = from emp in db.Employees.Where(empl => empl.Position == position)
                            select new
                            {
                                emp.Employee_ID,
                                emp.Surname,
                                emp.Name,
                                emp.Patronymic,
                                emp.Date_of_birth,
                                emp.Passport_Series,
                                emp.Passport_ID,
                                emp.Address,
                                emp.Phone,
                                emp.Hire_date
                            };
           
            return employees.ToList();
        }


       public short? ParseValidity(string validity)
        {
            short? year;
            try
            {
                year = (short)Int32.Parse(validity.Substring(0, 1));
            }
            catch
            {
                year = null;
            }
            return year;
        }
    }
}
