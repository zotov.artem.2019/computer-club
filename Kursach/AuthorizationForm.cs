﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach
{
    public partial class AuthorizationForm : Form
    {
        public AuthorizationForm()
        {
            InitializeComponent();
        }

        private void AuthorizationButton_Click(object sender, EventArgs e)
        {          
            if (LoginTextBox.Text != "" && maskedTextBoxPassword.Text != "")
            {
                ComputerClubModel context = new ComputerClubModel();
                if (context.Employees.Where(empl => empl.Login.Equals(LoginTextBox.Text)
                && empl.Password.Equals(maskedTextBoxPassword.Text)).Any())
                {
                    Employee admin;
                    admin = context.Employees.Where(empl => empl.Login.Equals(LoginTextBox.Text)
                && empl.Password.Equals(maskedTextBoxPassword.Text)).First();
                    this.Hide();
                    AdminForm adminForm = new AdminForm(admin);
                    adminForm.ShowDialog();
                }
                else if (context.Clients.Where(client => client.Login.Equals(LoginTextBox.Text)
                && client.Password.Equals(maskedTextBoxPassword.Text)).Any())
                {
                    Client cl;
                    cl = (Client)context.Clients.Where(client => client.Login.Equals(LoginTextBox.Text)
                && client.Password.Equals(maskedTextBoxPassword.Text)).First();
                    this.Hide();
                    ClientForm clientForm = new ClientForm(cl);
                    clientForm.ShowDialog();
                }
                else
                {
                    LoginTextBox.Clear();
                    maskedTextBoxPassword.Clear();
                    MessageBox.Show("Неправильный логин или пароль");
                }
            }
            else
            {
                MessageBox.Show("Поля не должны быть пустыми!");
            }
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.ShowDialog();
        }

        private void maskedTextBoxPassword_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassword.Select(maskedTextBoxPassword.Text.Length, 0);
        }
    }
}
