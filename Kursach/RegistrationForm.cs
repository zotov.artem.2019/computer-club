﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Kursach
{
    public partial class RegistrationForm : Form
    {
        private short clientId;
        private string name;
        private string surname;
        private string patronymic;
        private string login;
        private string password;
        private string passportSeries;
        private string passportId;
        private string address;
        private string phone;
        private DateTime dateOfBirth;
        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            AuthorizationForm authorizationForm = new AuthorizationForm();
            authorizationForm.ShowDialog();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            AddClient();
            this.Close();
            AuthorizationForm authorizationForm = new AuthorizationForm();
            authorizationForm.ShowDialog();
        }

        private void AddClient()
        {
            try
            {
                using (var db = new ComputerClubModel())
                {                   
                    clientId = (short) (db.Clients.Max(c => c.Client_ID) + 1);
                    name = textBoxName.Text.Trim();
                    surname = textBoxSurname.Text.Trim();
                    patronymic = textBoxPatronymic.Text.Trim();
                    login = textBoxLogin.Text.Trim();
                    password = maskedTextBoxPassword.Text.Trim();
                    passportSeries = maskedTextBoxPassportSeries.Text.Trim();
                    passportId = maskedTextBoxPassportId.Text.Trim();
                    address = textBoxAddress.Text.Trim();
                    phone = maskedTextBoxPhone.Text.Trim();

                    db.Clients.Add(new Client
                    {
                        Client_ID = clientId,
                        Surname = surname,
                        Name = name,
                        Patronymic = patronymic,
                        Login = login,
                        Password = password,
                        Passport_Series = passportSeries,
                        Passport_ID = passportId,
                        Address = address,
                        Phone = phone,
                        Date_of_birth = dateOfBirth
                    });
                    db.SaveChanges();
                }
                MessageBox.Show("Вы зарегистрировались!", "Добавлено", MessageBoxButtons.OK);
                this.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка в данных!", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void dateOfBirthPicker_Validating(object sender, CancelEventArgs e)
        {
            if ((DateTime.Now - dateOfBirthPicker.Value).Duration().TotalDays / 365 < 14)
            {
                errorProvider.SetError(dateOfBirthPicker, "Ошибка!");
                e.Cancel = true;
            }
            else
            {
                errorProvider.SetError(dateOfBirthPicker, String.Empty);
                e.Cancel = false;
            }
        }

        private void dateOfBirthPicker_Validated(object sender, EventArgs e)
        {
            dateOfBirth = dateOfBirthPicker.Value;
        }

        private void maskedTextBoxPassword_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassword.Select(maskedTextBoxPassword.Text.Length, 0);
        }

        private void maskedTextBoxPassportSeries_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassportSeries.Select(maskedTextBoxPassportSeries.Text.Length, 0);
        }

        private void maskedTextBoxPassportId_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassportId.Select(maskedTextBoxPassportId.Text.Length, 0);
        }
    }
}
