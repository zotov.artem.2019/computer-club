namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Computer parameters")]
    public partial class Computer_parameter
    {
        [Key]
        [Column("Parameters ID")]
        public byte Parameters_ID { get; set; }

        [Column("CPU frequency")]
        [Required]
        [StringLength(15)]
        public string CPU_frequency { get; set; }

        [Column("CPU threads count")]
        public byte CPU_threads_count { get; set; }

        [Required]
        [StringLength(6)]
        public string RAM { get; set; }

        [Column("Number of RAM modules")]
        public byte Number_of_RAM_modules { get; set; }

        [Column("Monitor frequency")]
        [Required]
        [StringLength(6)]
        public string Monitor_frequency { get; set; }

        [Column("Aviability of SSD")]
        [Required]
        [StringLength(4)]
        public string Aviability_of_SSD { get; set; }

        public virtual Computer_type Computer_type { get; set; }
    }
}
