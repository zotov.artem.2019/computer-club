namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Card type")]
    public partial class Card_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Card_type()
        {
            Club_card = new HashSet<Club_card>();
        }

        [Key]
        [Column("Type ID")]
        public byte Type_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public byte Discount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Club_card> Club_card { get; set; }
    }
}
