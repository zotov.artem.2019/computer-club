namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inspection")]
    public partial class Inspection
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Computer { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Employee { get; set; }

        [Key]
        [Column("Date of inspection", Order = 2, TypeName = "date")]
        public DateTime Date_of_inspection { get; set; }

        [Required]
        [StringLength(11)]
        public string Serviceability { get; set; }

        public virtual Computer Computer1 { get; set; }

        public virtual Employee Employee1 { get; set; }
    }
}
