namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            Club_card = new HashSet<Club_card>();
            Rents = new HashSet<Rent>();
        }

        [Key]
        [Column("Client ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Client_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [StringLength(30)]
        public string Patronymic { get; set; }

        [Required]
        [StringLength(15)]
        public string Login { get; set; }

        [Required]
        [StringLength(6)]
        public string Password { get; set; }

        [Column("Passport Series")]
        [Required]
        [StringLength(4)]
        public string Passport_Series { get; set; }

        [Column("Passport ID")]
        [Required]
        [StringLength(6)]
        public string Passport_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [Required]
        [StringLength(18)]
        public string Phone { get; set; }

        [Column("Date of birth", TypeName = "date")]
        public DateTime Date_of_birth { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Club_card> Club_card { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rent> Rents { get; set; }
    }
}
