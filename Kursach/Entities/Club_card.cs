namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Club card")]
    public partial class Club_card
    {
        [Key]
        [Column("Card ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Card_ID { get; set; }

        public byte Type { get; set; }

        public short Client { get; set; }

        [Column("Date of issue", TypeName = "date")]
        public DateTime Date_of_issue { get; set; }

        public short? Validity { get; set; }

        public virtual Card_type Card_type { get; set; }

        public virtual Client Client1 { get; set; }
    }
}
