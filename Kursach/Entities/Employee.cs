namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Inspections = new HashSet<Inspection>();
            Rents = new HashSet<Rent>();
        }

        [Key]
        [Column("Employee ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Employee_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [StringLength(30)]
        public string Patronymic { get; set; }

        public byte Position { get; set; }

        [StringLength(15)]
        public string Login { get; set; }

        [StringLength(6)]
        public string Password { get; set; }

        [Column("Date of birth", TypeName = "date")]
        public DateTime Date_of_birth { get; set; }

        [Column("Passport Series")]
        [Required]
        [StringLength(4)]
        public string Passport_Series { get; set; }

        [Column("Passport ID")]
        [Required]
        [StringLength(6)]
        public string Passport_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

        [Column("Hire date", TypeName = "date")]
        public DateTime Hire_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inspection> Inspections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rent> Rents { get; set; }

        public virtual Position Position1 { get; set; }
    }
}
