namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Computer type")]
    public partial class Computer_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Computer_type()
        {
            Computers = new HashSet<Computer>();
            Set_of_components = new HashSet<Set_of_component>();
            Computer_parameters = new HashSet<Computer_parameter>();
        }

        [Key]
        [Column("Type ID")]
        public byte Type_ID { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Column("Cost per hour", TypeName = "money")]
        public decimal Cost_per_hour { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Computer> Computers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Set_of_component> Set_of_components { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Computer_parameter> Computer_parameters { get; set; }
    }
}
