namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Rented computer")]
    public partial class Rented_computer
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Computer { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Rent { get; set; }

        [Key]
        [Column("Start date and time", Order = 2, TypeName = "smalldatetime")]
        public DateTime Start_date_and_time { get; set; }

        [Column("End date and time", TypeName = "smalldatetime")]
        public DateTime End_date_and_time { get; set; }

        public virtual Computer Computer1 { get; set; }

        public virtual Rent Rent1 { get; set; }
    }
}
