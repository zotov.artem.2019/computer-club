namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Computer")]
    public partial class Computer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Computer()
        {
            Inspections = new HashSet<Inspection>();
            Rented_computer = new HashSet<Rented_computer>();
        }

        [Key]
        [Column("Computer ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Computer_ID { get; set; }

        public byte Type { get; set; }

        public byte Components { get; set; }

        [Required]
        [StringLength(10)]
        public string OS { get; set; }

        [Required]
        [StringLength(30)]
        public string Keyboard { get; set; }

        [Required]
        [StringLength(30)]
        public string Mouse { get; set; }

        [Required]
        [StringLength(30)]
        public string Monitor { get; set; }

        [Required]
        [StringLength(30)]
        public string Headset { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inspection> Inspections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rented_computer> Rented_computer { get; set; }

        public virtual Computer_type Computer_type { get; set; }

        public virtual Set_of_component Set_of_components { get; set; }
    }
}
