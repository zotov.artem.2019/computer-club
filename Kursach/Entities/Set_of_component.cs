namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Set of components")]
    public partial class Set_of_component
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Set_of_component()
        {
            Computers = new HashSet<Computer>();
        }

        [Key]
        [Column("Set ID")]
        public byte Set_ID { get; set; }

        [StringLength(50)]
        public string GPU { get; set; }

        [Required]
        [StringLength(50)]
        public string CPU { get; set; }

        [Required]
        [StringLength(50)]
        public string Motherboard { get; set; }

        [Required]
        [StringLength(50)]
        public string RAM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Computer> Computers { get; set; }

        public virtual Computer_type Computer_type { get; set; }
    }
}
