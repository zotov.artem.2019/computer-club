namespace Kursach
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Rent")]
    public partial class Rent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rent()
        {
            Rented_computer = new HashSet<Rented_computer>();
        }

        [Key]
        [Column("Rent ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Rent_ID { get; set; }

        public short Client { get; set; }

        public short Employee { get; set; }

        [Column("Start date and time", TypeName = "smalldatetime")]
        public DateTime Start_date_and_time { get; set; }

        public TimeSpan Duration { get; set; }

        public virtual Client Client1 { get; set; }

        public virtual Employee Employee1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rented_computer> Rented_computer { get; set; }
    }
}
