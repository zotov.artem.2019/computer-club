using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Kursach
{
    public partial class ComputerClubModel : DbContext
    {
        public ComputerClubModel()
            : base("name=ComputerClubConfig")
        {
        }

        public virtual DbSet<Card_type> Card_types { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Club_card> Club_cards { get; set; }
        public virtual DbSet<Computer> Computers { get; set; }
        public virtual DbSet<Computer_parameter> Computer_parameters { get; set; }
        public virtual DbSet<Computer_type> Computer_types { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Inspection> Inspections { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Rent> Rents { get; set; }
        public virtual DbSet<Rented_computer> Rented_computers { get; set; }
        public virtual DbSet<Set_of_component> Set_of_components { get; set; }
        public virtual DbSet<Available_computer> Available_computers { get; set; }
        public virtual DbSet<Rent_info> Rent_infoes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Card_type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Card_type>()
                .HasMany(e => e.Club_card)
                .WithRequired(e => e.Card_type)
                .HasForeignKey(e => e.Type);

            modelBuilder.Entity<Client>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Patronymic)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Password)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .Property(e => e.Passport_Series)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .Property(e => e.Passport_ID)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Phone)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Club_card)
                .WithRequired(e => e.Client1)
                .HasForeignKey(e => e.Client);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Rents)
                .WithRequired(e => e.Client1)
                .HasForeignKey(e => e.Client);

            modelBuilder.Entity<Computer>()
                .Property(e => e.OS)
                .IsUnicode(false);

            modelBuilder.Entity<Computer>()
                .Property(e => e.Keyboard)
                .IsUnicode(false);

            modelBuilder.Entity<Computer>()
                .Property(e => e.Mouse)
                .IsUnicode(false);

            modelBuilder.Entity<Computer>()
                .Property(e => e.Monitor)
                .IsUnicode(false);

            modelBuilder.Entity<Computer>()
                .Property(e => e.Headset)
                .IsUnicode(false);

            modelBuilder.Entity<Computer>()
                .HasMany(e => e.Inspections)
                .WithRequired(e => e.Computer1)
                .HasForeignKey(e => e.Computer);

            modelBuilder.Entity<Computer>()
                .HasMany(e => e.Rented_computer)
                .WithRequired(e => e.Computer1)
                .HasForeignKey(e => e.Computer);

            modelBuilder.Entity<Computer_parameter>()
                .Property(e => e.CPU_frequency)
                .IsUnicode(false);

            modelBuilder.Entity<Computer_parameter>()
                .Property(e => e.RAM)
                .IsFixedLength();

            modelBuilder.Entity<Computer_parameter>()
                .Property(e => e.Monitor_frequency)
                .IsUnicode(false);

            modelBuilder.Entity<Computer_parameter>()
                .Property(e => e.Aviability_of_SSD)
                .IsUnicode(false);

            modelBuilder.Entity<Computer_type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Computer_type>()
                .Property(e => e.Cost_per_hour)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Computer_type>()
                .HasMany(e => e.Computers)
                .WithRequired(e => e.Computer_type)
                .HasForeignKey(e => e.Type);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Patronymic)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Password)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .Property(e => e.Passport_Series)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .Property(e => e.Passport_ID)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Phone)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Inspections)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.Employee);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Rents)
                .WithRequired(e => e.Employee1)
                .HasForeignKey(e => e.Employee);

            modelBuilder.Entity<Inspection>()
                .Property(e => e.Serviceability)
                .IsUnicode(false);

            modelBuilder.Entity<Position>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Position>()
                .Property(e => e.Salary)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Position>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Position1)
                .HasForeignKey(e => e.Position);

            modelBuilder.Entity<Rent>()
                .HasMany(e => e.Rented_computer)
                .WithRequired(e => e.Rent1)
                .HasForeignKey(e => e.Rent);

            modelBuilder.Entity<Set_of_component>()
                .Property(e => e.GPU)
                .IsUnicode(false);

            modelBuilder.Entity<Set_of_component>()
                .Property(e => e.CPU)
                .IsUnicode(false);

            modelBuilder.Entity<Set_of_component>()
                .Property(e => e.Motherboard)
                .IsUnicode(false);

            modelBuilder.Entity<Set_of_component>()
                .Property(e => e.RAM)
                .IsUnicode(false);

            modelBuilder.Entity<Set_of_component>()
                .HasMany(e => e.Computers)
                .WithRequired(e => e.Set_of_components)
                .HasForeignKey(e => e.Components);
        }
    }
}
