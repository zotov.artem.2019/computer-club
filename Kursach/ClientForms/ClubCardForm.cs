﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.ClientForms
{
    public partial class ClubCardForm : Form
    {
        private Client client;
        public ClubCardForm(Client client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClientForm clientForm = new ClientForm(client);
            clientForm.ShowDialog();
        }

        private void ClubCardForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void ClubCardForm_Load(object sender, EventArgs e)
        {
            SetCardGrid();
        }
        private void SetCardGrid()
        {
            ClientCardGridView.DataSource = GetCard();
            ClientCardGridView.Columns[0].HeaderText = "Номер карты";
            ClientCardGridView.Columns[1].HeaderText = "Тип";
            ClientCardGridView.Columns[2].HeaderText = "Клиент";
            ClientCardGridView.Columns[3].HeaderText = "Дата оформления";
            ClientCardGridView.Columns[4].HeaderText = "Срок действия";
        }

        public IEnumerable GetCard()
        {
            using (var db = new ComputerClubModel())
            {
                var cards = from card in db.Club_cards
                            select new
                            {
                                card.Card_ID,
                                card.Type,
                                card.Client,
                                card.Date_of_issue,
                                card.Validity
                            };
                return cards.Where(card => card.Client == client.Client_ID).ToList();
            }
            
        }
    }
}
