﻿namespace Kursach
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientForm));
            this.panelMainMenu = new System.Windows.Forms.Panel();
            this.buttonComputerList = new System.Windows.Forms.Button();
            this.buttonCurrentRents = new System.Windows.Forms.Button();
            this.buttonClubCards = new System.Windows.Forms.Button();
            this.userInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.labelYearsOld = new System.Windows.Forms.Label();
            this.labelClientLogin = new System.Windows.Forms.Label();
            this.logoutButton = new System.Windows.Forms.Button();
            this.userPicture = new System.Windows.Forms.PictureBox();
            this.userName = new System.Windows.Forms.Label();
            this.panelMainMenu.SuspendLayout();
            this.userInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMainMenu
            // 
            this.panelMainMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panelMainMenu.Controls.Add(this.buttonComputerList);
            this.panelMainMenu.Controls.Add(this.buttonCurrentRents);
            this.panelMainMenu.Controls.Add(this.buttonClubCards);
            this.panelMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMainMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMainMenu.Name = "panelMainMenu";
            this.panelMainMenu.Size = new System.Drawing.Size(225, 636);
            this.panelMainMenu.TabIndex = 3;
            // 
            // buttonComputerList
            // 
            this.buttonComputerList.FlatAppearance.BorderSize = 0;
            this.buttonComputerList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonComputerList.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonComputerList.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonComputerList.Image = ((System.Drawing.Image)(resources.GetObject("buttonComputerList.Image")));
            this.buttonComputerList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonComputerList.Location = new System.Drawing.Point(4, 112);
            this.buttonComputerList.Name = "buttonComputerList";
            this.buttonComputerList.Size = new System.Drawing.Size(218, 44);
            this.buttonComputerList.TabIndex = 3;
            this.buttonComputerList.Text = "Компьютеры";
            this.buttonComputerList.UseVisualStyleBackColor = true;
            this.buttonComputerList.Click += new System.EventHandler(this.buttonComputerList_Click);
            // 
            // buttonCurrentRents
            // 
            this.buttonCurrentRents.FlatAppearance.BorderSize = 0;
            this.buttonCurrentRents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCurrentRents.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCurrentRents.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCurrentRents.Image = ((System.Drawing.Image)(resources.GetObject("buttonCurrentRents.Image")));
            this.buttonCurrentRents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCurrentRents.Location = new System.Drawing.Point(4, 62);
            this.buttonCurrentRents.Name = "buttonCurrentRents";
            this.buttonCurrentRents.Size = new System.Drawing.Size(218, 44);
            this.buttonCurrentRents.TabIndex = 2;
            this.buttonCurrentRents.Text = "Мои аренды";
            this.buttonCurrentRents.UseVisualStyleBackColor = true;
            this.buttonCurrentRents.Click += new System.EventHandler(this.buttonRents_Click);
            // 
            // buttonClubCards
            // 
            this.buttonClubCards.FlatAppearance.BorderSize = 0;
            this.buttonClubCards.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClubCards.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonClubCards.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClubCards.Image = ((System.Drawing.Image)(resources.GetObject("buttonClubCards.Image")));
            this.buttonClubCards.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClubCards.Location = new System.Drawing.Point(4, 12);
            this.buttonClubCards.Name = "buttonClubCards";
            this.buttonClubCards.Size = new System.Drawing.Size(218, 44);
            this.buttonClubCards.TabIndex = 1;
            this.buttonClubCards.Text = "Моя карта";
            this.buttonClubCards.UseVisualStyleBackColor = true;
            this.buttonClubCards.Click += new System.EventHandler(this.buttonClubCards_Click);
            // 
            // userInfoGroupBox
            // 
            this.userInfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userInfoGroupBox.Controls.Add(this.labelYearsOld);
            this.userInfoGroupBox.Controls.Add(this.labelClientLogin);
            this.userInfoGroupBox.Controls.Add(this.logoutButton);
            this.userInfoGroupBox.Controls.Add(this.userPicture);
            this.userInfoGroupBox.Controls.Add(this.userName);
            this.userInfoGroupBox.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userInfoGroupBox.Location = new System.Drawing.Point(228, 0);
            this.userInfoGroupBox.Name = "userInfoGroupBox";
            this.userInfoGroupBox.Size = new System.Drawing.Size(859, 207);
            this.userInfoGroupBox.TabIndex = 4;
            this.userInfoGroupBox.TabStop = false;
            this.userInfoGroupBox.Text = "Информация о пользователе";
            // 
            // labelYearsOld
            // 
            this.labelYearsOld.AutoSize = true;
            this.labelYearsOld.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelYearsOld.Location = new System.Drawing.Point(185, 109);
            this.labelYearsOld.Name = "labelYearsOld";
            this.labelYearsOld.Size = new System.Drawing.Size(62, 19);
            this.labelYearsOld.TabIndex = 3;
            this.labelYearsOld.Text = "Возраст";
            // 
            // labelClientLogin
            // 
            this.labelClientLogin.AutoSize = true;
            this.labelClientLogin.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClientLogin.Location = new System.Drawing.Point(185, 36);
            this.labelClientLogin.Name = "labelClientLogin";
            this.labelClientLogin.Size = new System.Drawing.Size(25, 19);
            this.labelClientLogin.TabIndex = 2;
            this.labelClientLogin.Text = "ID";
            // 
            // logoutButton
            // 
            this.logoutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.logoutButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.logoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.logoutButton.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logoutButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.logoutButton.Location = new System.Drawing.Point(726, 157);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(123, 44);
            this.logoutButton.TabIndex = 4;
            this.logoutButton.Text = "Выйти";
            this.logoutButton.UseVisualStyleBackColor = false;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // userPicture
            // 
            this.userPicture.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.userPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userPicture.Image = ((System.Drawing.Image)(resources.GetObject("userPicture.Image")));
            this.userPicture.Location = new System.Drawing.Point(6, 25);
            this.userPicture.Name = "userPicture";
            this.userPicture.Size = new System.Drawing.Size(155, 150);
            this.userPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.userPicture.TabIndex = 1;
            this.userPicture.TabStop = false;
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userName.Location = new System.Drawing.Point(185, 73);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(45, 19);
            this.userName.TabIndex = 0;
            this.userName.Text = "ФИО";
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1092, 636);
            this.Controls.Add(this.userInfoGroupBox);
            this.Controls.Add(this.panelMainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Окно клиента";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientForm_FormClosing);
            this.Load += new System.EventHandler(this.ClientForm_Load);
            this.panelMainMenu.ResumeLayout(false);
            this.userInfoGroupBox.ResumeLayout(false);
            this.userInfoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMainMenu;
        private System.Windows.Forms.Button buttonComputerList;
        private System.Windows.Forms.Button buttonCurrentRents;
        private System.Windows.Forms.Button buttonClubCards;
        private System.Windows.Forms.GroupBox userInfoGroupBox;
        private System.Windows.Forms.Label labelYearsOld;
        private System.Windows.Forms.Label labelClientLogin;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.PictureBox userPicture;
        private System.Windows.Forms.Label userName;
    }
}