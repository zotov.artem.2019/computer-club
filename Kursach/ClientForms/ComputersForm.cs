﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.ClientForms
{
    public partial class ComputersForm : Form
    {
        private Client client;
        private bool buttonFlag = false;
        public ComputersForm(Client client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void ComputersForm_Load(object sender, EventArgs e)
        {
            SetComputersGrid();
        }
        public IEnumerable GetComputers()
        {
            using (var db = new ComputerClubModel())
            {
                var computers = from computer in db.Computers.Join(db.Computer_types,
                    c => c.Type,
                    t => t.Type_ID,
                    (c,t) => new
                    {
                        c.Computer_ID,
                        t.Name,
                        c.Components,
                        c.OS,
                        c.Keyboard,
                        c.Mouse,
                        c.Monitor,
                        c.Headset
                    })
                                select new
                                {
                                    computer.Computer_ID,
                                    computer.Name,
                                    computer.Components,
                                    computer.OS,
                                    computer.Keyboard,
                                    computer.Mouse,
                                    computer.Monitor,
                                    computer.Headset

                                };
                return computers.ToList();
            }
        }
        private void SetComputersGrid()
        {
            computersGridView.DataSource = GetComputers();
            computersGridView.Columns[0].HeaderText = "Номер компьютера";
            computersGridView.Columns[1].HeaderText = "Тип";
            computersGridView.Columns[2].HeaderText = "Комплектующие";
            computersGridView.Columns[3].HeaderText = "ОС";
            computersGridView.Columns[4].HeaderText = "Клавиатура";
            computersGridView.Columns[5].HeaderText = "Мышь";
            computersGridView.Columns[6].HeaderText = "Монитор";
            computersGridView.Columns[7].HeaderText = "Наушники";
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClientForm clientForm = new ClientForm(client);
            clientForm.ShowDialog();
        }

        private void ComputersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void ComponentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetComponentsGrid();
        }

        private void SetComponentsGrid()
        {
            dataGridViewComponents.DataSource = GetComponents();
            dataGridViewComponents.Columns[0].HeaderText = "Номер набора";
            dataGridViewComponents.Columns[1].HeaderText = "Видеокарта";
            dataGridViewComponents.Columns[2].HeaderText = "Процессор";
            dataGridViewComponents.Columns[3].HeaderText = "Материнская плата";
            dataGridViewComponents.Columns[4].HeaderText = "Оперативная память";
        }

        private IEnumerable GetComponents()
        {
            using (var db = new ComputerClubModel())
            {
                var i = computersGridView.SelectedCells[0].OwningRow.Index;
                short compId = (short)computersGridView[0, i].Value;
                var components = from component in db.Set_of_components
                                select new
                                {
                                    component.Set_ID,
                                    component.GPU,
                                    component.CPU,
                                    component.Motherboard,
                                    component.RAM
                                };
                return components.Where(set => set.Set_ID == 
                db.Computers.Where(pc => pc.Computer_ID == compId).FirstOrDefault().Components).ToList();
            }
        }

        private void buttonAviablePC_Click(object sender, EventArgs e)
        {
            if (buttonFlag == false)
            {
                buttonFlag = true;
                buttonAviablePC.Text = "Отобразить все";
                SetAviableComputersGrid();
            } else
            {   
                buttonFlag = false;
                SetComputersGrid();
                buttonAviablePC.Text = "Отобразить свободные";
            }
            
        }

        private void SetAviableComputersGrid()
        {
            computersGridView.DataSource = GetAviableComputers();
            computersGridView.Columns[0].HeaderText = "Номер компьютера";
            computersGridView.Columns[1].HeaderText = "Тип";
            computersGridView.Columns[2].HeaderText = "Комплектующие";
            computersGridView.Columns[3].HeaderText = "ОС";
            computersGridView.Columns[4].HeaderText = "Клавиатура";
            computersGridView.Columns[5].HeaderText = "Мышь";
            computersGridView.Columns[6].HeaderText = "Монитор";
            computersGridView.Columns[7].HeaderText = "Наушники";

        }

        private IEnumerable GetAviableComputers()
        {
            using (var db = new ComputerClubModel())
            {              
                List<Computer> aviable = new List<Computer>();
                foreach (var comp in db.Available_computers)
                {
                    aviable.Add(db.Computers.Where(pc => pc.Computer_ID == comp.Computer_ID).FirstOrDefault());                    
                };
                var aviablePC = from computer in aviable.Join(db.Computer_types,
                    c => c.Type,
                    t => t.Type_ID,
                    (c, t) => new
                    {
                        c.Computer_ID,
                        t.Name,
                        c.Components,
                        c.OS,
                        c.Keyboard,
                        c.Mouse,
                        c.Monitor,
                        c.Headset
                    })
                                select new
                                {
                                    computer.Computer_ID,
                                    computer.Name,
                                    computer.Components,
                                    computer.OS,
                                    computer.Keyboard,
                                    computer.Mouse,
                                    computer.Monitor,
                                    computer.Headset
                                };
                
                return aviablePC.ToList();
            }
        }


    }
}
