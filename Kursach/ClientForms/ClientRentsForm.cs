﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.ClientForms
{
    public partial class ClientRentsForm : Form
    {
        private Client client;
        public ClientRentsForm(Client client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void ClientRentsForm_Load(object sender, EventArgs e)
        {
            SetRentsGrid();
        }
        private void SetRentsGrid()
        {
            ClientRentsGridView.DataSource = GetClientRents();
            ClientRentsGridView.Columns[0].HeaderText = "Номер аренды";
            ClientRentsGridView.Columns[1].HeaderText = "Клиент";
            ClientRentsGridView.Columns[2].HeaderText = "Компьютер";
            ClientRentsGridView.Columns[3].HeaderText = "Сотрудник";
            ClientRentsGridView.Columns[4].HeaderText = "Время начала";
            ClientRentsGridView.Columns[5].HeaderText = "Длительность";
            ClientRentsGridView.Columns[6].HeaderText = "Время окончания";
        }

        private IEnumerable GetClientRents()
        {
            using (var db = new ComputerClubModel())
            {
                var rents = from rent in db.Rents.Join(db.Rented_computers,
                    r => r.Rent_ID,
                    c => c.Rent,
                    (r, c) => new
                    {
                        r.Rent_ID,
                        r.Client,
                        c.Computer,
                        r.Employee,
                        r.Start_date_and_time,
                        r.Duration,
                        c.End_date_and_time
                    })
                            select new
                            {
                                rent.Rent_ID,
                                rent.Client,
                                rent.Computer,
                                rent.Employee,
                                rent.Start_date_and_time,
                                rent.Duration,
                                rent.End_date_and_time
                            };

                return rents.Where(r => r.Client == client.Client_ID).ToList();
            }
        }

        private void ClientRentsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClientForm clientForm = new ClientForm(client);
            clientForm.ShowDialog();
        }
    }
}
