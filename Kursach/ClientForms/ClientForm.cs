﻿
using Kursach.ClientForms;
using System;
using System.Windows.Forms;

namespace Kursach
{
    public partial class ClientForm : Form
    {
        private Client client;
        public ClientForm(Client client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            AuthorizationForm authorizationForm = new AuthorizationForm();
            this.Hide();
            authorizationForm.ShowDialog();
        }

        private void buttonComputerList_Click(object sender, EventArgs e)
        {
            ComputersForm computersForm = new ComputersForm(client);
            this.Hide();
            computersForm.ShowDialog();
        }

        private void buttonRents_Click(object sender, EventArgs e)
        {
            ClientRentsForm clientRentsForm = new ClientRentsForm(client);
            this.Hide();
            clientRentsForm.ShowDialog();
        }

        private void buttonClubCards_Click(object sender, EventArgs e)
        {
            ClientForms.ClubCardForm clubCardForm = new ClientForms.ClubCardForm(client);
            this.Hide();
            clubCardForm.ShowDialog();
        }

        private void ClientForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            userName.Text = "ФИО: " + client.Surname + " " + client.Name + " " + client.Patronymic;
            labelClientLogin.Text = "Логин: " + client.Login.ToString();
            labelYearsOld.Text = "Возраст " + Math.Floor((DateTime.Now - client.Date_of_birth).TotalDays / 365).ToString();
        }
    }
}
