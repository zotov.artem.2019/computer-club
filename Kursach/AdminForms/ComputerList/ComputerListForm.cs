﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.ComputerList
{
    public partial class ComputerListForm : Form
    {
        Employee admin;
        public ComputerListForm(Employee admin)
        {
            InitializeComponent();
            this.admin = admin;   
        }
        public IEnumerable GetComputers()
        {
            using (var db = new ComputerClubModel())
            {
                var computers = from computer in db.Computers
                              select new
                              {
                                  computer.Computer_ID,
                                  computer.Type,
                                  computer.Components,
                                  computer.OS,
                                  computer.Keyboard,
                                  computer.Mouse,
                                  computer.Monitor,
                                  computer.Headset

                              };
                return computers.ToList();
            }
        }
        private void SetComputersGrid()
        {
            CompGridView.DataSource = GetComputers();
            CompGridView.Columns[0].HeaderText = "Номер компьютера";
            CompGridView.Columns[1].HeaderText = "Тип";
            CompGridView.Columns[2].HeaderText = "Комплектующие";
            CompGridView.Columns[3].HeaderText = "ОС";
            CompGridView.Columns[4].HeaderText = "Клавиатура";
            CompGridView.Columns[5].HeaderText = "Мышь";
            CompGridView.Columns[6].HeaderText = "Монитор";
            CompGridView.Columns[7].HeaderText = "Наушники";
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminForm adminForm = new AdminForm(admin);
            adminForm.ShowDialog();
        }

        private void ComputerListForm_Load(object sender, EventArgs e)
        {
            SetComputersGrid();
        }

        private void addClientMenuItem_Click(object sender, EventArgs e)
        {
            AddCompForm add = new AddCompForm();
            if (add.ShowDialog(this) == DialogResult.OK)
            {
                SetComputersGrid();
            }
        }

        private void updateMenuItem_Click(object sender, EventArgs e)
        {
            if (CompGridView.SelectedCells.Count > 0)
            {
                var i = CompGridView.SelectedCells[0].OwningRow.Index;
                short compId = (short)CompGridView[0, i].Value;
                UpdateCompForm update = new UpdateCompForm(compId);
                if (update.ShowDialog(this) == DialogResult.OK)
                {
                    SetComputersGrid();
                }
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите удалить этот элемент?",
                "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                if (CompGridView.SelectedCells.Count > 0)
                {
                    var i = CompGridView.SelectedCells[0].OwningRow.Index;
                    short compId = (short)CompGridView[0, i].Value;
                    using (var db = new ComputerClubModel())
                    {
                        Computer comp = db.Computers.Where(x => x.Computer_ID == compId).First();
                        db.Computers.Remove(comp);
                        db.SaveChanges();
                    }
                }
            }
            SetComputersGrid();
        }

        private void refreshMenuItem_Click(object sender, EventArgs e)
        {
            SetComputersGrid();
        }

        private void ComputerListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void addToBrokenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
