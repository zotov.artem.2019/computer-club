﻿namespace Kursach.AdminForms.ComputerList
{
    partial class ComputerListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComputerListForm));
            this.btnBack = new System.Windows.Forms.Button();
            this.CompContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addClientMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainerComp = new System.Windows.Forms.SplitContainer();
            this.CompGridView = new System.Windows.Forms.DataGridView();
            this.CompContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerComp)).BeginInit();
            this.splitContainerComp.Panel1.SuspendLayout();
            this.splitContainerComp.Panel2.SuspendLayout();
            this.splitContainerComp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CompGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBack.Location = new System.Drawing.Point(2, 2);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(146, 47);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Назад";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // CompContextMenu
            // 
            this.CompContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addClientMenuItem,
            this.updateMenuItem,
            this.deleteMenuItem,
            this.refreshMenuItem});
            this.CompContextMenu.Name = "ClientContextMenu";
            this.CompContextMenu.Size = new System.Drawing.Size(129, 92);
            // 
            // addClientMenuItem
            // 
            this.addClientMenuItem.Name = "addClientMenuItem";
            this.addClientMenuItem.Size = new System.Drawing.Size(128, 22);
            this.addClientMenuItem.Text = "Добавить";
            this.addClientMenuItem.Click += new System.EventHandler(this.addClientMenuItem_Click);
            // 
            // updateMenuItem
            // 
            this.updateMenuItem.Name = "updateMenuItem";
            this.updateMenuItem.Size = new System.Drawing.Size(128, 22);
            this.updateMenuItem.Text = "Изменить";
            this.updateMenuItem.Click += new System.EventHandler(this.updateMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(128, 22);
            this.deleteMenuItem.Text = "Удалить";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // refreshMenuItem
            // 
            this.refreshMenuItem.Name = "refreshMenuItem";
            this.refreshMenuItem.Size = new System.Drawing.Size(128, 22);
            this.refreshMenuItem.Text = "Обновить";
            this.refreshMenuItem.Click += new System.EventHandler(this.refreshMenuItem_Click);
            // 
            // splitContainerComp
            // 
            this.splitContainerComp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerComp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainerComp.Location = new System.Drawing.Point(0, -1);
            this.splitContainerComp.Name = "splitContainerComp";
            // 
            // splitContainerComp.Panel1
            // 
            this.splitContainerComp.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainerComp.Panel1.Controls.Add(this.btnBack);
            // 
            // splitContainerComp.Panel2
            // 
            this.splitContainerComp.Panel2.Controls.Add(this.CompGridView);
            this.splitContainerComp.Size = new System.Drawing.Size(1092, 636);
            this.splitContainerComp.SplitterDistance = 154;
            this.splitContainerComp.TabIndex = 42;
            // 
            // CompGridView
            // 
            this.CompGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CompGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CompGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.CompGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CompGridView.ContextMenuStrip = this.CompContextMenu;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CompGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.CompGridView.Location = new System.Drawing.Point(-5, -2);
            this.CompGridView.Name = "CompGridView";
            this.CompGridView.Size = new System.Drawing.Size(937, 634);
            this.CompGridView.TabIndex = 2;
            // 
            // ComputerListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBack;
            this.ClientSize = new System.Drawing.Size(1092, 636);
            this.Controls.Add(this.splitContainerComp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComputerListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Компьютеры";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ComputerListForm_FormClosing);
            this.Load += new System.EventHandler(this.ComputerListForm_Load);
            this.CompContextMenu.ResumeLayout(false);
            this.splitContainerComp.Panel1.ResumeLayout(false);
            this.splitContainerComp.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerComp)).EndInit();
            this.splitContainerComp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CompGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.ContextMenuStrip CompContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addClientMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshMenuItem;
        private System.Windows.Forms.SplitContainer splitContainerComp;
        private System.Windows.Forms.DataGridView CompGridView;
    }
}