﻿namespace Kursach.AdminForms.ComputerList
{
    partial class AddCompForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCompForm));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddComp = new System.Windows.Forms.Button();
            this.componentsBox = new System.Windows.Forms.ComboBox();
            this.lblHeadset = new System.Windows.Forms.Label();
            this.lblMonitor = new System.Windows.Forms.Label();
            this.lblMouse = new System.Windows.Forms.Label();
            this.lblKeyboard = new System.Windows.Forms.Label();
            this.lblOS = new System.Windows.Forms.Label();
            this.lblComponents = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.textBoxHeadset = new System.Windows.Forms.TextBox();
            this.textBoxMonitor = new System.Windows.Forms.TextBox();
            this.textBoxMouse = new System.Windows.Forms.TextBox();
            this.osBox = new System.Windows.Forms.ComboBox();
            this.textBoxKeyboard = new System.Windows.Forms.TextBox();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.panelAddComp = new System.Windows.Forms.Panel();
            this.labelType = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.groupBoxButtons = new System.Windows.Forms.GroupBox();
            this.textBoxCompId = new System.Windows.Forms.TextBox();
            this.labelCompId = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panelAddComp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.groupBoxButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(326, 18);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 35);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddComp
            // 
            this.btnAddComp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddComp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnAddComp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddComp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAddComp.Location = new System.Drawing.Point(180, 18);
            this.btnAddComp.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddComp.Name = "btnAddComp";
            this.btnAddComp.Size = new System.Drawing.Size(125, 34);
            this.btnAddComp.TabIndex = 9;
            this.btnAddComp.Text = "Добавить";
            this.btnAddComp.UseVisualStyleBackColor = false;
            this.btnAddComp.Click += new System.EventHandler(this.btnAddComp_Click);
            // 
            // componentsBox
            // 
            this.componentsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.componentsBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.componentsBox.FormattingEnabled = true;
            this.componentsBox.Location = new System.Drawing.Point(12, 150);
            this.componentsBox.Name = "componentsBox";
            this.componentsBox.Size = new System.Drawing.Size(193, 29);
            this.componentsBox.TabIndex = 3;
            // 
            // lblHeadset
            // 
            this.lblHeadset.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblHeadset.AutoSize = true;
            this.lblHeadset.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeadset.Location = new System.Drawing.Point(9, 602);
            this.lblHeadset.Name = "lblHeadset";
            this.lblHeadset.Size = new System.Drawing.Size(85, 21);
            this.lblHeadset.TabIndex = 54;
            this.lblHeadset.Text = "Наушники";
            // 
            // lblMonitor
            // 
            this.lblMonitor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMonitor.Location = new System.Drawing.Point(8, 489);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(76, 21);
            this.lblMonitor.TabIndex = 53;
            this.lblMonitor.Text = "Монитор";
            // 
            // lblMouse
            // 
            this.lblMouse.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMouse.AutoSize = true;
            this.lblMouse.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMouse.Location = new System.Drawing.Point(11, 373);
            this.lblMouse.Name = "lblMouse";
            this.lblMouse.Size = new System.Drawing.Size(56, 21);
            this.lblMouse.TabIndex = 52;
            this.lblMouse.Text = "Мышь";
            // 
            // lblKeyboard
            // 
            this.lblKeyboard.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblKeyboard.AutoSize = true;
            this.lblKeyboard.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblKeyboard.Location = new System.Drawing.Point(9, 256);
            this.lblKeyboard.Name = "lblKeyboard";
            this.lblKeyboard.Size = new System.Drawing.Size(92, 21);
            this.lblKeyboard.TabIndex = 51;
            this.lblKeyboard.Text = "Клавиатура";
            // 
            // lblOS
            // 
            this.lblOS.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblOS.AutoSize = true;
            this.lblOS.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOS.Location = new System.Drawing.Point(8, 190);
            this.lblOS.Name = "lblOS";
            this.lblOS.Size = new System.Drawing.Size(179, 21);
            this.lblOS.TabIndex = 50;
            this.lblOS.Text = "Операционная система";
            // 
            // lblComponents
            // 
            this.lblComponents.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblComponents.AutoSize = true;
            this.lblComponents.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblComponents.Location = new System.Drawing.Point(9, 126);
            this.lblComponents.Name = "lblComponents";
            this.lblComponents.Size = new System.Drawing.Size(130, 21);
            this.lblComponents.TabIndex = 49;
            this.lblComponents.Text = "Комплектующие";
            // 
            // lblType
            // 
            this.lblType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(341, 117);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(26, 13);
            this.lblType.TabIndex = 48;
            this.lblType.Text = "Тип";
            // 
            // textBoxHeadset
            // 
            this.textBoxHeadset.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxHeadset.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxHeadset.Location = new System.Drawing.Point(12, 624);
            this.textBoxHeadset.Multiline = true;
            this.textBoxHeadset.Name = "textBoxHeadset";
            this.textBoxHeadset.Size = new System.Drawing.Size(193, 82);
            this.textBoxHeadset.TabIndex = 8;
            // 
            // textBoxMonitor
            // 
            this.textBoxMonitor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxMonitor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMonitor.Location = new System.Drawing.Point(12, 512);
            this.textBoxMonitor.Multiline = true;
            this.textBoxMonitor.Name = "textBoxMonitor";
            this.textBoxMonitor.Size = new System.Drawing.Size(193, 82);
            this.textBoxMonitor.TabIndex = 7;
            // 
            // textBoxMouse
            // 
            this.textBoxMouse.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxMouse.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMouse.Location = new System.Drawing.Point(12, 395);
            this.textBoxMouse.Multiline = true;
            this.textBoxMouse.Name = "textBoxMouse";
            this.textBoxMouse.Size = new System.Drawing.Size(193, 82);
            this.textBoxMouse.TabIndex = 6;
            // 
            // osBox
            // 
            this.osBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.osBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.osBox.FormattingEnabled = true;
            this.osBox.Location = new System.Drawing.Point(12, 214);
            this.osBox.Name = "osBox";
            this.osBox.Size = new System.Drawing.Size(193, 29);
            this.osBox.TabIndex = 4;
            // 
            // textBoxKeyboard
            // 
            this.textBoxKeyboard.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxKeyboard.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxKeyboard.Location = new System.Drawing.Point(12, 280);
            this.textBoxKeyboard.Multiline = true;
            this.textBoxKeyboard.Name = "textBoxKeyboard";
            this.textBoxKeyboard.Size = new System.Drawing.Size(193, 82);
            this.textBoxKeyboard.TabIndex = 5;
            // 
            // comboBoxType
            // 
            this.comboBoxType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxType.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(12, 87);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(193, 29);
            this.comboBoxType.TabIndex = 2;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.AutoScroll = true;
            this.splitContainer.Panel1.Controls.Add(this.panelAddComp);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.pictureBoxLogo);
            this.splitContainer.Panel2.Controls.Add(this.groupBoxButtons);
            this.splitContainer.Size = new System.Drawing.Size(712, 454);
            this.splitContainer.SplitterDistance = 237;
            this.splitContainer.TabIndex = 60;
            // 
            // panelAddComp
            // 
            this.panelAddComp.Controls.Add(this.textBoxCompId);
            this.panelAddComp.Controls.Add(this.labelCompId);
            this.panelAddComp.Controls.Add(this.labelType);
            this.panelAddComp.Controls.Add(this.textBoxKeyboard);
            this.panelAddComp.Controls.Add(this.textBoxMouse);
            this.panelAddComp.Controls.Add(this.comboBoxType);
            this.panelAddComp.Controls.Add(this.textBoxMonitor);
            this.panelAddComp.Controls.Add(this.osBox);
            this.panelAddComp.Controls.Add(this.lblHeadset);
            this.panelAddComp.Controls.Add(this.componentsBox);
            this.panelAddComp.Controls.Add(this.textBoxHeadset);
            this.panelAddComp.Controls.Add(this.lblComponents);
            this.panelAddComp.Controls.Add(this.lblMonitor);
            this.panelAddComp.Controls.Add(this.lblOS);
            this.panelAddComp.Controls.Add(this.lblKeyboard);
            this.panelAddComp.Controls.Add(this.lblMouse);
            this.panelAddComp.Location = new System.Drawing.Point(0, 3);
            this.panelAddComp.Name = "panelAddComp";
            this.panelAddComp.Size = new System.Drawing.Size(219, 751);
            this.panelAddComp.TabIndex = 61;
            // 
            // labelType
            // 
            this.labelType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelType.AutoSize = true;
            this.labelType.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelType.Location = new System.Drawing.Point(9, 63);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(36, 21);
            this.labelType.TabIndex = 60;
            this.labelType.Text = "Тип";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(-2, 0);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(473, 390);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 69;
            this.pictureBoxLogo.TabStop = false;
            // 
            // groupBoxButtons
            // 
            this.groupBoxButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxButtons.Controls.Add(this.btnCancel);
            this.groupBoxButtons.Controls.Add(this.btnAddComp);
            this.groupBoxButtons.Location = new System.Drawing.Point(3, 387);
            this.groupBoxButtons.Name = "groupBoxButtons";
            this.groupBoxButtons.Size = new System.Drawing.Size(467, 67);
            this.groupBoxButtons.TabIndex = 68;
            this.groupBoxButtons.TabStop = false;
            // 
            // textBoxCompId
            // 
            this.textBoxCompId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxCompId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCompId.Location = new System.Drawing.Point(12, 26);
            this.textBoxCompId.Name = "textBoxCompId";
            this.textBoxCompId.Size = new System.Drawing.Size(193, 29);
            this.textBoxCompId.TabIndex = 0;
            this.textBoxCompId.TabStop = false;
            this.textBoxCompId.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxCompId_Validating);
            this.textBoxCompId.Validated += new System.EventHandler(this.textBoxCompId_Validated);
            // 
            // labelCompId
            // 
            this.labelCompId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCompId.AutoSize = true;
            this.labelCompId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCompId.Location = new System.Drawing.Point(9, 2);
            this.labelCompId.Name = "labelCompId";
            this.labelCompId.Size = new System.Drawing.Size(152, 21);
            this.labelCompId.TabIndex = 62;
            this.labelCompId.Text = "Номер компьютера";
            // 
            // AddCompForm
            // 
            this.AcceptButton = this.btnAddComp;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(712, 454);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.lblType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddCompForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить компьютер";
            this.Load += new System.EventHandler(this.AddCompForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panelAddComp.ResumeLayout(false);
            this.panelAddComp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.groupBoxButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddComp;
        private System.Windows.Forms.ComboBox componentsBox;
        private System.Windows.Forms.Label lblHeadset;
        private System.Windows.Forms.Label lblMonitor;
        private System.Windows.Forms.Label lblMouse;
        private System.Windows.Forms.Label lblKeyboard;
        private System.Windows.Forms.Label lblOS;
        private System.Windows.Forms.Label lblComponents;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.TextBox textBoxHeadset;
        private System.Windows.Forms.TextBox textBoxMonitor;
        private System.Windows.Forms.TextBox textBoxMouse;
        private System.Windows.Forms.ComboBox osBox;
        private System.Windows.Forms.TextBox textBoxKeyboard;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panelAddComp;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.GroupBox groupBoxButtons;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.TextBox textBoxCompId;
        private System.Windows.Forms.Label labelCompId;
    }
}