﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Kursach.AdminForms.ComputerList
{
    public partial class AddCompForm : Form
    {
        private short computerID;
        private byte type;
        private byte pcComponents;
        private string os;
        private string keyboard;
        private string mouse;
        private string monitor;
        private string headset;
        public AddCompForm()
        {
            InitializeComponent();
        }
   
        private void btnAddComp_Click(object sender, EventArgs e)
        {
            AddComputer();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Close();
        }

        private void AddComputer()
        {
            try
            {
                using (var db = new ComputerClubModel())
                {
                    type = db.Computer_types.Where(
                c => c.Name == comboBoxType.SelectedItem.ToString()).First().Type_ID;
                    pcComponents = (byte)Convert.ToInt32(componentsBox.SelectedItem);
                    os = osBox.SelectedItem.ToString().Trim();
                    keyboard = textBoxKeyboard.Text.Trim();
                    mouse = textBoxMouse.Text.Trim();
                    monitor = textBoxMonitor.Text.Trim();
                    headset = textBoxHeadset.Text.Trim();

                    db.Computers.Add(new Computer
                    {
                      Computer_ID = computerID,
                                 Type = type,
                                  Components = pcComponents,
                                  OS = os,
                                  Keyboard = keyboard,
                                  Mouse = mouse,
                                  Monitor = monitor,
                                  Headset = headset
                    });
                    db.SaveChanges();
                }
                MessageBox.Show("Данные добавлены!", "Добавлено", MessageBoxButtons.OK);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка в данных!", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void AddCompForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                List<Computer_type> types = new List<Computer_type>();
                types.AddRange(db.Computer_types);
                foreach (var type in types)
                {
                    comboBoxType.Items.Add(type.Name);
                }

                int[] count = new int[db.Set_of_components.Count()];
                for (int i = 0; i < count.Length; i++)
                {
                    count[i] = i + 1;
                    componentsBox.Items.Add(count[i]);
                }
                osBox.Items.Add("Windows");
                osBox.Items.Add("Linux");
                osBox.Items.Add("Mac OS");
            }
        }

        private void textBoxCompId_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string input = textBoxCompId.Text.Trim();
            using (var db = new ComputerClubModel())
            {
                if (db.Computers.Where(cl => cl.Computer_ID.ToString() == input).Any())
                {
                    errorProvider.SetError(textBoxCompId, "Компьютер с таким номером уже существует!");
                    e.Cancel = true;
                }
                else if (Regex.IsMatch(input, @"(?<=\s|^)\d+(?=\s|$)"))
                {
                    errorProvider.SetError(textBoxCompId, String.Empty);
                    e.Cancel = false;
                }
                else
                {
                    errorProvider.SetError(textBoxCompId, "Ошибка!");
                    e.Cancel = true;
                }
            }
        }

        private void textBoxCompId_Validated(object sender, EventArgs e)
        {
            computerID = (short)Convert.ToInt32(textBoxCompId.Text.Trim());
        }
    }
}
