﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.ComputerList
{
    public partial class UpdateCompForm : Form
    {
        private short computerID;
        private byte type;
        private byte pcComponents;
        private string os;
        private string keyboard;
        private string mouse;
        private string monitor;
        private string headset;

        public UpdateCompForm(short computerID)
        {
            InitializeComponent();
            this.computerID = computerID;
        }

        private void UpdateComp()
        {   
            int compId = this.computerID;
            using (var db = new ComputerClubModel())
            {
                type = db.Computer_types.Where(
                    c => c.Name == comboBoxType.SelectedItem.ToString()).First().Type_ID;
                pcComponents = (byte)Convert.ToInt32(componentsBox.SelectedItem);
                os = osBox.SelectedItem.ToString().Trim();
                keyboard = textBoxKeyboard.Text.Trim();
                mouse = textBoxMouse.Text.Trim();
                monitor = textBoxMonitor.Text.Trim();
                headset = textBoxHeadset.Text.Trim();

                Computer pc = db.Computers.Where(x => x.Computer_ID == compId).First();
                pc.Type = type;
                pc.Components = pcComponents;
                pc.OS = os;
                pc.Keyboard = keyboard;
                pc.Mouse = mouse;
                pc.Monitor = monitor;
                pc.Headset = headset;
                db.SaveChanges();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите изменить этот элемент?",
                "Изменение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            DialogResult = ValidateChildren() ? DialogResult.OK : DialogResult.None;
            if (DialogResult == DialogResult.OK)
            {
                UpdateComp();
            }
            this.Close();
        }

        private void UpdateCompForm_Load(object sender, EventArgs e)
        {
            textBoxCompId.Text = computerID.ToString();
            using (var db = new ComputerClubModel())
            {
                List<Computer_type> types = new List<Computer_type>();
                types.AddRange(db.Computer_types);
                foreach (var type in types)
                {
                    comboBoxType.Items.Add(type.Name);
                }
        
                int[] count = new int[db.Set_of_components.Count()];
                for (int i = 0; i < count.Length; i++)
                {
                    count[i] = i + 1;
                    componentsBox.Items.Add(count[i]);
                }                                                     
                osBox.Items.Add("Windows");
                osBox.Items.Add("Linux");
                osBox.Items.Add("Mac OS");

                Computer computer = db.Computers.Where(c => c.Computer_ID == computerID).First();
                comboBoxType.Text = db.Computer_types.Where(type => type.Type_ID == computer.Type).First().Name;
                componentsBox.Text = computer.Components.ToString();
                osBox.Text = computer.OS;
                textBoxHeadset.Text = computer.Headset;
                textBoxKeyboard.Text = computer.Keyboard;
                textBoxMonitor.Text = computer.Monitor;
                textBoxMouse.Text = computer.Mouse;
            }               
        }
    }
}
