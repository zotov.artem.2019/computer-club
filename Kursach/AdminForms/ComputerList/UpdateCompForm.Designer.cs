﻿namespace Kursach.AdminForms.ComputerList
{
    partial class UpdateCompForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateCompForm));
            this.textBoxCompId = new System.Windows.Forms.TextBox();
            this.textBoxKeyboard = new System.Windows.Forms.TextBox();
            this.osBox = new System.Windows.Forms.ComboBox();
            this.textBoxMouse = new System.Windows.Forms.TextBox();
            this.textBoxMonitor = new System.Windows.Forms.TextBox();
            this.textBoxHeadset = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblComponents = new System.Windows.Forms.Label();
            this.lblOS = new System.Windows.Forms.Label();
            this.lblKeyboard = new System.Windows.Forms.Label();
            this.lblMouse = new System.Windows.Forms.Label();
            this.lblMonitor = new System.Windows.Forms.Label();
            this.lblHeadset = new System.Windows.Forms.Label();
            this.labelCompId = new System.Windows.Forms.Label();
            this.componentsBox = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.panelUpdateComp = new System.Windows.Forms.Panel();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.groupBoxButtons = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panelUpdateComp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.groupBoxButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxCompId
            // 
            this.textBoxCompId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxCompId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCompId.Location = new System.Drawing.Point(13, 31);
            this.textBoxCompId.Name = "textBoxCompId";
            this.textBoxCompId.ReadOnly = true;
            this.textBoxCompId.Size = new System.Drawing.Size(184, 29);
            this.textBoxCompId.TabIndex = 0;
            this.textBoxCompId.TabStop = false;
            // 
            // textBoxKeyboard
            // 
            this.textBoxKeyboard.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxKeyboard.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxKeyboard.Location = new System.Drawing.Point(13, 398);
            this.textBoxKeyboard.Multiline = true;
            this.textBoxKeyboard.Name = "textBoxKeyboard";
            this.textBoxKeyboard.Size = new System.Drawing.Size(184, 82);
            this.textBoxKeyboard.TabIndex = 5;
            // 
            // osBox
            // 
            this.osBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.osBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.osBox.FormattingEnabled = true;
            this.osBox.Location = new System.Drawing.Point(13, 220);
            this.osBox.Name = "osBox";
            this.osBox.Size = new System.Drawing.Size(184, 29);
            this.osBox.TabIndex = 3;
            // 
            // textBoxMouse
            // 
            this.textBoxMouse.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxMouse.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMouse.Location = new System.Drawing.Point(13, 517);
            this.textBoxMouse.Multiline = true;
            this.textBoxMouse.Name = "textBoxMouse";
            this.textBoxMouse.Size = new System.Drawing.Size(184, 82);
            this.textBoxMouse.TabIndex = 6;
            // 
            // textBoxMonitor
            // 
            this.textBoxMonitor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxMonitor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMonitor.Location = new System.Drawing.Point(15, 630);
            this.textBoxMonitor.Multiline = true;
            this.textBoxMonitor.Name = "textBoxMonitor";
            this.textBoxMonitor.Size = new System.Drawing.Size(182, 82);
            this.textBoxMonitor.TabIndex = 7;
            // 
            // textBoxHeadset
            // 
            this.textBoxHeadset.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxHeadset.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxHeadset.Location = new System.Drawing.Point(13, 279);
            this.textBoxHeadset.Multiline = true;
            this.textBoxHeadset.Name = "textBoxHeadset";
            this.textBoxHeadset.Size = new System.Drawing.Size(184, 82);
            this.textBoxHeadset.TabIndex = 4;
            // 
            // lblType
            // 
            this.lblType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblType.Location = new System.Drawing.Point(9, 68);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(36, 21);
            this.lblType.TabIndex = 9;
            this.lblType.Text = "Тип";
            // 
            // lblComponents
            // 
            this.lblComponents.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblComponents.AutoSize = true;
            this.lblComponents.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblComponents.Location = new System.Drawing.Point(10, 130);
            this.lblComponents.Name = "lblComponents";
            this.lblComponents.Size = new System.Drawing.Size(130, 21);
            this.lblComponents.TabIndex = 10;
            this.lblComponents.Text = "Комплектующие";
            // 
            // lblOS
            // 
            this.lblOS.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblOS.AutoSize = true;
            this.lblOS.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOS.Location = new System.Drawing.Point(10, 196);
            this.lblOS.Name = "lblOS";
            this.lblOS.Size = new System.Drawing.Size(179, 21);
            this.lblOS.TabIndex = 11;
            this.lblOS.Text = "Операционная система";
            // 
            // lblKeyboard
            // 
            this.lblKeyboard.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblKeyboard.AutoSize = true;
            this.lblKeyboard.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblKeyboard.Location = new System.Drawing.Point(9, 374);
            this.lblKeyboard.Name = "lblKeyboard";
            this.lblKeyboard.Size = new System.Drawing.Size(92, 21);
            this.lblKeyboard.TabIndex = 12;
            this.lblKeyboard.Text = "Клавиатура";
            // 
            // lblMouse
            // 
            this.lblMouse.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMouse.AutoSize = true;
            this.lblMouse.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMouse.Location = new System.Drawing.Point(10, 497);
            this.lblMouse.Name = "lblMouse";
            this.lblMouse.Size = new System.Drawing.Size(56, 21);
            this.lblMouse.TabIndex = 13;
            this.lblMouse.Text = "Мышь";
            // 
            // lblMonitor
            // 
            this.lblMonitor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMonitor.Location = new System.Drawing.Point(12, 609);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(76, 21);
            this.lblMonitor.TabIndex = 14;
            this.lblMonitor.Text = "Монитор";
            // 
            // lblHeadset
            // 
            this.lblHeadset.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblHeadset.AutoSize = true;
            this.lblHeadset.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeadset.Location = new System.Drawing.Point(10, 255);
            this.lblHeadset.Name = "lblHeadset";
            this.lblHeadset.Size = new System.Drawing.Size(85, 21);
            this.lblHeadset.TabIndex = 15;
            this.lblHeadset.Text = "Наушники";
            // 
            // labelCompId
            // 
            this.labelCompId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCompId.AutoSize = true;
            this.labelCompId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCompId.Location = new System.Drawing.Point(10, 7);
            this.labelCompId.Name = "labelCompId";
            this.labelCompId.Size = new System.Drawing.Size(152, 21);
            this.labelCompId.TabIndex = 16;
            this.labelCompId.Text = "Номер компьютера";
            // 
            // componentsBox
            // 
            this.componentsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.componentsBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.componentsBox.FormattingEnabled = true;
            this.componentsBox.Location = new System.Drawing.Point(13, 154);
            this.componentsBox.Name = "componentsBox";
            this.componentsBox.Size = new System.Drawing.Size(184, 29);
            this.componentsBox.TabIndex = 2;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUpdate.Location = new System.Drawing.Point(182, 19);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(125, 34);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "Изменить";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(328, 19);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 35);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // comboBoxType
            // 
            this.comboBoxType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxType.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(13, 92);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(184, 29);
            this.comboBoxType.TabIndex = 1;
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer.Location = new System.Drawing.Point(1, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.AutoScroll = true;
            this.splitContainer.Panel1.Controls.Add(this.panelUpdateComp);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.pictureBoxLogo);
            this.splitContainer.Panel2.Controls.Add(this.groupBoxButtons);
            this.splitContainer.Size = new System.Drawing.Size(712, 454);
            this.splitContainer.SplitterDistance = 237;
            this.splitContainer.TabIndex = 42;
            // 
            // panelUpdateComp
            // 
            this.panelUpdateComp.Controls.Add(this.textBoxKeyboard);
            this.panelUpdateComp.Controls.Add(this.componentsBox);
            this.panelUpdateComp.Controls.Add(this.textBoxMouse);
            this.panelUpdateComp.Controls.Add(this.lblHeadset);
            this.panelUpdateComp.Controls.Add(this.textBoxCompId);
            this.panelUpdateComp.Controls.Add(this.textBoxMonitor);
            this.panelUpdateComp.Controls.Add(this.osBox);
            this.panelUpdateComp.Controls.Add(this.lblMonitor);
            this.panelUpdateComp.Controls.Add(this.comboBoxType);
            this.panelUpdateComp.Controls.Add(this.lblKeyboard);
            this.panelUpdateComp.Controls.Add(this.textBoxHeadset);
            this.panelUpdateComp.Controls.Add(this.lblMouse);
            this.panelUpdateComp.Controls.Add(this.labelCompId);
            this.panelUpdateComp.Controls.Add(this.lblComponents);
            this.panelUpdateComp.Controls.Add(this.lblType);
            this.panelUpdateComp.Controls.Add(this.lblOS);
            this.panelUpdateComp.Location = new System.Drawing.Point(3, 3);
            this.panelUpdateComp.Name = "panelUpdateComp";
            this.panelUpdateComp.Size = new System.Drawing.Size(211, 751);
            this.panelUpdateComp.TabIndex = 43;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(-1, -2);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(473, 390);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 68;
            this.pictureBoxLogo.TabStop = false;
            // 
            // groupBoxButtons
            // 
            this.groupBoxButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxButtons.Controls.Add(this.btnUpdate);
            this.groupBoxButtons.Controls.Add(this.btnCancel);
            this.groupBoxButtons.Location = new System.Drawing.Point(-1, 380);
            this.groupBoxButtons.Name = "groupBoxButtons";
            this.groupBoxButtons.Size = new System.Drawing.Size(467, 67);
            this.groupBoxButtons.TabIndex = 67;
            this.groupBoxButtons.TabStop = false;
            // 
            // UpdateCompForm
            // 
            this.AcceptButton = this.btnUpdate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(712, 454);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateCompForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Изменить данные";
            this.Load += new System.EventHandler(this.UpdateCompForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panelUpdateComp.ResumeLayout(false);
            this.panelUpdateComp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.groupBoxButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCompId;
        private System.Windows.Forms.TextBox textBoxKeyboard;
        private System.Windows.Forms.ComboBox osBox;
        private System.Windows.Forms.TextBox textBoxMouse;
        private System.Windows.Forms.TextBox textBoxMonitor;
        private System.Windows.Forms.TextBox textBoxHeadset;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblComponents;
        private System.Windows.Forms.Label lblOS;
        private System.Windows.Forms.Label lblKeyboard;
        private System.Windows.Forms.Label lblMouse;
        private System.Windows.Forms.Label lblMonitor;
        private System.Windows.Forms.Label lblHeadset;
        private System.Windows.Forms.Label labelCompId;
        private System.Windows.Forms.ComboBox componentsBox;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panelUpdateComp;
        private System.Windows.Forms.GroupBox groupBoxButtons;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
    }
}