﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.Employees
{
    public partial class UpdateEmployeeForm : Form
    {
        private short employeeID;
        private byte posId;
        private string surname;
        private string name;
        private string patronymic;
        private DateTime hireDate;
        private DateTime birthDate;
        private string phone;
        private string address;
        private string passportSeries;
        private string passportId;
        public UpdateEmployeeForm(short employeeID)
        {
            InitializeComponent();
            this.employeeID = employeeID;
        }

        private void UpdateEmployee()
        {
            
                using (var db = new ComputerClubModel())
                {
                    patronymic = textBoxPatronymic.Text.Trim();
                    name = textBoxName.Text.Trim();
                    surname = textBoxSurname.Text.Trim();
                    passportSeries = maskedTextBoxPassportSeries.Text.Trim();
                    passportId = maskedTextBoxPassportId.Text.Trim();
                    address = textBoxAddress.Text.Trim();
                    birthDate = dateOfBirthPicker.Value;
                    phone = maskedTextBoxPhone.Text.Trim();
                    hireDate = dateTimePickerHireDate.Value;
                    posId = db.Positions.Where(pos => pos.Name == comboBoxPosition.Text.ToString()).First().Position_ID;

                    Employee employee = db.Employees.Where(x => x.Employee_ID == employeeID).First();
                    employee.Surname = surname;
                    employee.Name = name;
                    employee.Patronymic = patronymic;
                    employee.Passport_Series = passportSeries;
                    employee.Passport_ID = passportId;
                    employee.Address = address;
                    employee.Phone = phone;
                    employee.Date_of_birth = birthDate;
                    employee.Hire_date = hireDate;
                    employee.Position = posId;
                    db.SaveChanges();
                }
            
                  
        }

        private void UpdateEmployeeForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                Employee employee = db.Employees.Where(empl => empl.Employee_ID == employeeID).First();
                textBoxAddress.Text = employee.Address;
                textBoxEmployeeId.Text = employee.Employee_ID.ToString();
                textBoxSurname.Text = employee.Surname;
                textBoxName.Text = employee.Name;
                textBoxPatronymic.Text = employee.Patronymic;
                comboBoxPosition.Text = db.Positions.Where(pos => pos.Position_ID == employee.Position).First().Name;
                dateOfBirthPicker.Value = employee.Date_of_birth;
                dateTimePickerHireDate.Value = employee.Hire_date;
                maskedTextBoxPassportId.Text = employee.Passport_ID;
                maskedTextBoxPassportSeries.Text = employee.Passport_Series;
                maskedTextBoxPhone.Text = employee.Phone;
                foreach (Position position in db.Positions)
                {
                    if (position.Name != "Администратор")
                    {
                        comboBoxPosition.Items.Add(position.Name);
                    }
                    
                }             
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите изменить этот элемент?",
                "Изменение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            DialogResult = ValidateChildren() ? DialogResult.OK : DialogResult.None;
            if (DialogResult == DialogResult.OK)
            {
                UpdateEmployee();
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            Close();
        }

        private void maskedTextBoxPhone_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPhone.SelectionStart = 0;
        }
    }
}
