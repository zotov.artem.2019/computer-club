﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Kursach.AdminForms.Employees
{
    public partial class AddEmployeeForm : Form
    {
        private byte posId;
        private string surname;
        private string name;
        private string patronymic;
        private DateTime hireDate;
        private DateTime birthDate;
        private short employeeId;
        private string phone;
        private string address;
        private string passportSeries;
        private string passportId;
        public AddEmployeeForm()
        {
            InitializeComponent();
        }

        private void AddEmployee()
        {
            
                using (var db = new ComputerClubModel())
                {
                    posId = db.Positions.Where(pos => pos.Name == comboBoxPosition.SelectedItem.ToString()).First().Position_ID;
                    surname = textBoxSurname.Text.Trim();
                    name = textBoxName.Text.Trim();
                    patronymic = textBoxPatronymic.Text.Trim();
                    hireDate = dateTimePickerHireDate.Value;
                    birthDate = dateOfBirthPicker.Value;
                    phone = maskedTextBoxPhone.Text;
                    address = textBoxAddress.Text.Trim();
                    passportSeries = maskedTextBoxPassportSeries.Text.Trim();
                    passportId = maskedTextBoxPassportId.Text.Trim();
                    
                    db.Employees.Add(new Employee
                    {
                        Employee_ID = employeeId,
                        Surname = surname,
                        Name = name,
                        Patronymic = patronymic,
                        Address = address,
                        Passport_ID = passportId,
                        Passport_Series = passportSeries,
                        Position = posId,
                        Password = null,
                        Login = null, 
                        Hire_date = hireDate,
                        Date_of_birth = birthDate,
                        Phone = phone
                    });
                    db.SaveChanges();
                }
                MessageBox.Show("Данные добавлены!", "Добавлено", MessageBoxButtons.OK);
            
           
        }

        private void AddEmployeeForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                List<Position> positions = new List<Position>();
                positions.AddRange(db.Positions);
                foreach (Position position in positions)
                {
                    if (position.Name != "Администратор")
                    {
                        comboBoxPosition.Items.Add(position.Name);
                    }
                    
                }
                   
            }
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddEmployee();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Close();
        }

        private void maskedTextBoxPassportSeries_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassportSeries.SelectionStart = 0;
        }

        private void maskedTextBoxPassportId_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassportId.SelectionStart = 0;
        }

        private void maskedTextBoxPhone_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPhone.SelectionStart = 4;

        }

        private void textBoxEmployeeId_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string input = textBoxEmployeeId.Text.Trim();
            using (var db = new ComputerClubModel())
            {
                if (db.Employees.Where(cl => cl.Employee_ID.ToString() == input).Any())
                {
                    errorProvider.SetError(textBoxEmployeeId, "Сотрудник с таким номером уже существует!");
                    e.Cancel = true;
                }
                else if (Regex.IsMatch(input, @"(?<=\s|^)\d+(?=\s|$)"))
                {
                    errorProvider.SetError(textBoxEmployeeId, String.Empty);
                    e.Cancel = false;
                }
                else
                {
                    errorProvider.SetError(textBoxEmployeeId, "Ошибка!");
                    e.Cancel = true;
                }
            }
        }

        private void textBoxEmployeeId_Validated(object sender, EventArgs e)
        {
            employeeId = (short)Convert.ToInt32(textBoxEmployeeId.Text.Trim());
        }
    }
}
