﻿namespace Kursach
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.userInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.labelStazh = new System.Windows.Forms.Label();
            this.labelYearsOld = new System.Windows.Forms.Label();
            this.labelEmployeeId = new System.Windows.Forms.Label();
            this.logoutButton = new System.Windows.Forms.Button();
            this.userPicture = new System.Windows.Forms.PictureBox();
            this.userName = new System.Windows.Forms.Label();
            this.panelMainMenu = new System.Windows.Forms.Panel();
            this.buttonComputerList = new System.Windows.Forms.Button();
            this.buttonCurrentRents = new System.Windows.Forms.Button();
            this.buttonClubCards = new System.Windows.Forms.Button();
            this.buttonClientList = new System.Windows.Forms.Button();
            this.panelEmployees = new System.Windows.Forms.Panel();
            this.tabControlEmployees = new System.Windows.Forms.TabControl();
            this.tabPageMasters = new System.Windows.Forms.TabPage();
            this.dataGridViewPCMasters = new System.Windows.Forms.DataGridView();
            this.EmployeeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addRentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPageCleaners = new System.Windows.Forms.TabPage();
            this.dataGridViewCleaners = new System.Windows.Forms.DataGridView();
            this.tabPageSecurity = new System.Windows.Forms.TabPage();
            this.dataGridViewSecurity = new System.Windows.Forms.DataGridView();
            this.userInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userPicture)).BeginInit();
            this.panelMainMenu.SuspendLayout();
            this.panelEmployees.SuspendLayout();
            this.tabControlEmployees.SuspendLayout();
            this.tabPageMasters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPCMasters)).BeginInit();
            this.EmployeeContextMenu.SuspendLayout();
            this.tabPageCleaners.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCleaners)).BeginInit();
            this.tabPageSecurity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSecurity)).BeginInit();
            this.SuspendLayout();
            // 
            // userInfoGroupBox
            // 
            this.userInfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userInfoGroupBox.Controls.Add(this.labelStazh);
            this.userInfoGroupBox.Controls.Add(this.labelYearsOld);
            this.userInfoGroupBox.Controls.Add(this.labelEmployeeId);
            this.userInfoGroupBox.Controls.Add(this.logoutButton);
            this.userInfoGroupBox.Controls.Add(this.userPicture);
            this.userInfoGroupBox.Controls.Add(this.userName);
            this.userInfoGroupBox.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userInfoGroupBox.Location = new System.Drawing.Point(231, 0);
            this.userInfoGroupBox.Name = "userInfoGroupBox";
            this.userInfoGroupBox.Size = new System.Drawing.Size(859, 207);
            this.userInfoGroupBox.TabIndex = 0;
            this.userInfoGroupBox.TabStop = false;
            this.userInfoGroupBox.Text = "Информация о пользователе";
            // 
            // labelStazh
            // 
            this.labelStazh.AutoSize = true;
            this.labelStazh.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStazh.Location = new System.Drawing.Point(185, 148);
            this.labelStazh.Name = "labelStazh";
            this.labelStazh.Size = new System.Drawing.Size(45, 19);
            this.labelStazh.TabIndex = 4;
            this.labelStazh.Text = "Стаж";
            // 
            // labelYearsOld
            // 
            this.labelYearsOld.AutoSize = true;
            this.labelYearsOld.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelYearsOld.Location = new System.Drawing.Point(185, 109);
            this.labelYearsOld.Name = "labelYearsOld";
            this.labelYearsOld.Size = new System.Drawing.Size(62, 19);
            this.labelYearsOld.TabIndex = 3;
            this.labelYearsOld.Text = "Возраст";
            // 
            // labelEmployeeId
            // 
            this.labelEmployeeId.AutoSize = true;
            this.labelEmployeeId.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEmployeeId.Location = new System.Drawing.Point(185, 36);
            this.labelEmployeeId.Name = "labelEmployeeId";
            this.labelEmployeeId.Size = new System.Drawing.Size(25, 19);
            this.labelEmployeeId.TabIndex = 2;
            this.labelEmployeeId.Text = "ID";
            // 
            // logoutButton
            // 
            this.logoutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.logoutButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.logoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.logoutButton.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logoutButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.logoutButton.Location = new System.Drawing.Point(726, 157);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(123, 44);
            this.logoutButton.TabIndex = 4;
            this.logoutButton.Text = "Выйти";
            this.logoutButton.UseVisualStyleBackColor = false;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // userPicture
            // 
            this.userPicture.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.userPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userPicture.Image = ((System.Drawing.Image)(resources.GetObject("userPicture.Image")));
            this.userPicture.Location = new System.Drawing.Point(6, 25);
            this.userPicture.Name = "userPicture";
            this.userPicture.Size = new System.Drawing.Size(155, 150);
            this.userPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.userPicture.TabIndex = 1;
            this.userPicture.TabStop = false;
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userName.Location = new System.Drawing.Point(185, 73);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(45, 19);
            this.userName.TabIndex = 0;
            this.userName.Text = "ФИО";
            // 
            // panelMainMenu
            // 
            this.panelMainMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panelMainMenu.Controls.Add(this.buttonComputerList);
            this.panelMainMenu.Controls.Add(this.buttonCurrentRents);
            this.panelMainMenu.Controls.Add(this.buttonClubCards);
            this.panelMainMenu.Controls.Add(this.buttonClientList);
            this.panelMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMainMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMainMenu.Name = "panelMainMenu";
            this.panelMainMenu.Size = new System.Drawing.Size(225, 636);
            this.panelMainMenu.TabIndex = 2;
            // 
            // buttonComputerList
            // 
            this.buttonComputerList.FlatAppearance.BorderSize = 0;
            this.buttonComputerList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonComputerList.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonComputerList.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonComputerList.Image = ((System.Drawing.Image)(resources.GetObject("buttonComputerList.Image")));
            this.buttonComputerList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonComputerList.Location = new System.Drawing.Point(4, 162);
            this.buttonComputerList.Name = "buttonComputerList";
            this.buttonComputerList.Size = new System.Drawing.Size(218, 44);
            this.buttonComputerList.TabIndex = 3;
            this.buttonComputerList.Text = "Компьютеры";
            this.buttonComputerList.UseVisualStyleBackColor = true;
            this.buttonComputerList.Click += new System.EventHandler(this.buttonComputerList_Click);
            // 
            // buttonCurrentRents
            // 
            this.buttonCurrentRents.FlatAppearance.BorderSize = 0;
            this.buttonCurrentRents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCurrentRents.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCurrentRents.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCurrentRents.Image = ((System.Drawing.Image)(resources.GetObject("buttonCurrentRents.Image")));
            this.buttonCurrentRents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCurrentRents.Location = new System.Drawing.Point(4, 112);
            this.buttonCurrentRents.Name = "buttonCurrentRents";
            this.buttonCurrentRents.Size = new System.Drawing.Size(218, 44);
            this.buttonCurrentRents.TabIndex = 2;
            this.buttonCurrentRents.Text = "Текущие аренды";
            this.buttonCurrentRents.UseVisualStyleBackColor = true;
            this.buttonCurrentRents.Click += new System.EventHandler(this.buttonCurrentRents_Click);
            // 
            // buttonClubCards
            // 
            this.buttonClubCards.FlatAppearance.BorderSize = 0;
            this.buttonClubCards.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClubCards.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonClubCards.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClubCards.Image = ((System.Drawing.Image)(resources.GetObject("buttonClubCards.Image")));
            this.buttonClubCards.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClubCards.Location = new System.Drawing.Point(4, 62);
            this.buttonClubCards.Name = "buttonClubCards";
            this.buttonClubCards.Size = new System.Drawing.Size(218, 44);
            this.buttonClubCards.TabIndex = 1;
            this.buttonClubCards.Text = "Клубные карты";
            this.buttonClubCards.UseVisualStyleBackColor = true;
            this.buttonClubCards.Click += new System.EventHandler(this.buttonClubCards_Click);
            // 
            // buttonClientList
            // 
            this.buttonClientList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonClientList.FlatAppearance.BorderSize = 0;
            this.buttonClientList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClientList.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonClientList.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClientList.Image = ((System.Drawing.Image)(resources.GetObject("buttonClientList.Image")));
            this.buttonClientList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClientList.Location = new System.Drawing.Point(4, 12);
            this.buttonClientList.Name = "buttonClientList";
            this.buttonClientList.Size = new System.Drawing.Size(218, 44);
            this.buttonClientList.TabIndex = 0;
            this.buttonClientList.Text = "Список клиентов";
            this.buttonClientList.UseVisualStyleBackColor = false;
            this.buttonClientList.Click += new System.EventHandler(this.buttonClientList_Click);
            // 
            // panelEmployees
            // 
            this.panelEmployees.Controls.Add(this.tabControlEmployees);
            this.panelEmployees.Location = new System.Drawing.Point(231, 213);
            this.panelEmployees.Name = "panelEmployees";
            this.panelEmployees.Size = new System.Drawing.Size(859, 423);
            this.panelEmployees.TabIndex = 3;
            // 
            // tabControlEmployees
            // 
            this.tabControlEmployees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlEmployees.Controls.Add(this.tabPageMasters);
            this.tabControlEmployees.Controls.Add(this.tabPageCleaners);
            this.tabControlEmployees.Controls.Add(this.tabPageSecurity);
            this.tabControlEmployees.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControlEmployees.Location = new System.Drawing.Point(0, 0);
            this.tabControlEmployees.Name = "tabControlEmployees";
            this.tabControlEmployees.SelectedIndex = 0;
            this.tabControlEmployees.Size = new System.Drawing.Size(859, 420);
            this.tabControlEmployees.TabIndex = 6;
            this.tabControlEmployees.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControlEmployees_Selecting);
            // 
            // tabPageMasters
            // 
            this.tabPageMasters.Controls.Add(this.dataGridViewPCMasters);
            this.tabPageMasters.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabPageMasters.Location = new System.Drawing.Point(4, 24);
            this.tabPageMasters.Name = "tabPageMasters";
            this.tabPageMasters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMasters.Size = new System.Drawing.Size(851, 392);
            this.tabPageMasters.TabIndex = 0;
            this.tabPageMasters.Text = "Компьютерные мастера";
            this.tabPageMasters.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPCMasters
            // 
            this.dataGridViewPCMasters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPCMasters.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPCMasters.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPCMasters.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPCMasters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPCMasters.ContextMenuStrip = this.EmployeeContextMenu;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPCMasters.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewPCMasters.Location = new System.Drawing.Point(-1, -1);
            this.dataGridViewPCMasters.Name = "dataGridViewPCMasters";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPCMasters.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewPCMasters.Size = new System.Drawing.Size(852, 395);
            this.dataGridViewPCMasters.TabIndex = 2;
            // 
            // EmployeeContextMenu
            // 
            this.EmployeeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRentMenuItem,
            this.updateMenuItem,
            this.deleteMenuItem,
            this.refreshMenuItem});
            this.EmployeeContextMenu.Name = "ClientContextMenu";
            this.EmployeeContextMenu.Size = new System.Drawing.Size(129, 92);
            // 
            // addRentMenuItem
            // 
            this.addRentMenuItem.Name = "addRentMenuItem";
            this.addRentMenuItem.Size = new System.Drawing.Size(128, 22);
            this.addRentMenuItem.Text = "Добавить";
            this.addRentMenuItem.Click += new System.EventHandler(this.addEmployeeMenuItem_Click);
            // 
            // updateMenuItem
            // 
            this.updateMenuItem.Name = "updateMenuItem";
            this.updateMenuItem.Size = new System.Drawing.Size(128, 22);
            this.updateMenuItem.Text = "Изменить";
            this.updateMenuItem.Click += new System.EventHandler(this.updateMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(128, 22);
            this.deleteMenuItem.Text = "Удалить";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // refreshMenuItem
            // 
            this.refreshMenuItem.Name = "refreshMenuItem";
            this.refreshMenuItem.Size = new System.Drawing.Size(128, 22);
            this.refreshMenuItem.Text = "Обновить";
            this.refreshMenuItem.Click += new System.EventHandler(this.refreshMenuItem_Click);
            // 
            // tabPageCleaners
            // 
            this.tabPageCleaners.Controls.Add(this.dataGridViewCleaners);
            this.tabPageCleaners.Location = new System.Drawing.Point(4, 24);
            this.tabPageCleaners.Name = "tabPageCleaners";
            this.tabPageCleaners.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCleaners.Size = new System.Drawing.Size(851, 392);
            this.tabPageCleaners.TabIndex = 1;
            this.tabPageCleaners.Text = "Уборщики";
            this.tabPageCleaners.UseVisualStyleBackColor = true;
            // 
            // dataGridViewCleaners
            // 
            this.dataGridViewCleaners.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCleaners.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCleaners.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewCleaners.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCleaners.ContextMenuStrip = this.EmployeeContextMenu;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCleaners.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewCleaners.Location = new System.Drawing.Point(-1, -1);
            this.dataGridViewCleaners.Name = "dataGridViewCleaners";
            this.dataGridViewCleaners.Size = new System.Drawing.Size(852, 395);
            this.dataGridViewCleaners.TabIndex = 1;
            // 
            // tabPageSecurity
            // 
            this.tabPageSecurity.Controls.Add(this.dataGridViewSecurity);
            this.tabPageSecurity.Location = new System.Drawing.Point(4, 24);
            this.tabPageSecurity.Name = "tabPageSecurity";
            this.tabPageSecurity.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSecurity.Size = new System.Drawing.Size(851, 392);
            this.tabPageSecurity.TabIndex = 2;
            this.tabPageSecurity.Text = "Охранники";
            this.tabPageSecurity.UseVisualStyleBackColor = true;
            // 
            // dataGridViewSecurity
            // 
            this.dataGridViewSecurity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSecurity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSecurity.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewSecurity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSecurity.ContextMenuStrip = this.EmployeeContextMenu;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewSecurity.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewSecurity.Location = new System.Drawing.Point(-1, -1);
            this.dataGridViewSecurity.Name = "dataGridViewSecurity";
            this.dataGridViewSecurity.Size = new System.Drawing.Size(852, 395);
            this.dataGridViewSecurity.TabIndex = 1;
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1092, 636);
            this.Controls.Add(this.panelEmployees);
            this.Controls.Add(this.panelMainMenu);
            this.Controls.Add(this.userInfoGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Окно администратора";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminForm_FormClosing);
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.userInfoGroupBox.ResumeLayout(false);
            this.userInfoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userPicture)).EndInit();
            this.panelMainMenu.ResumeLayout(false);
            this.panelEmployees.ResumeLayout(false);
            this.tabControlEmployees.ResumeLayout(false);
            this.tabPageMasters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPCMasters)).EndInit();
            this.EmployeeContextMenu.ResumeLayout(false);
            this.tabPageCleaners.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCleaners)).EndInit();
            this.tabPageSecurity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSecurity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox userInfoGroupBox;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.PictureBox userPicture;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Panel panelMainMenu;
        private System.Windows.Forms.Button buttonComputerList;
        private System.Windows.Forms.Button buttonCurrentRents;
        private System.Windows.Forms.Button buttonClubCards;
        private System.Windows.Forms.Button buttonClientList;
        private System.Windows.Forms.Label labelStazh;
        private System.Windows.Forms.Label labelYearsOld;
        private System.Windows.Forms.Label labelEmployeeId;
        private System.Windows.Forms.Panel panelEmployees;
        private System.Windows.Forms.TabControl tabControlEmployees;
        private System.Windows.Forms.TabPage tabPageMasters;
        private System.Windows.Forms.TabPage tabPageCleaners;
        private System.Windows.Forms.TabPage tabPageSecurity;
        private System.Windows.Forms.DataGridView dataGridViewCleaners;
        private System.Windows.Forms.DataGridView dataGridViewSecurity;
        private System.Windows.Forms.DataGridView dataGridViewPCMasters;
        private System.Windows.Forms.ContextMenuStrip EmployeeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addRentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshMenuItem;
    }
}