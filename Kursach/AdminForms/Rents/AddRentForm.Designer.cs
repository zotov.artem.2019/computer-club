﻿namespace Kursach.AdminForms.Rents
{
    partial class AddRentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddRentForm));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.panelAddRentButtons = new System.Windows.Forms.Panel();
            this.labelDuration = new System.Windows.Forms.Label();
            this.labelComp = new System.Windows.Forms.Label();
            this.labelClient = new System.Windows.Forms.Label();
            this.maskedTextBoxDuration = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxComp = new System.Windows.Forms.ComboBox();
            this.comboBoxClient = new System.Windows.Forms.ComboBox();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.groupBoxButtons = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddRent = new System.Windows.Forms.Button();
            this.labelRentId = new System.Windows.Forms.Label();
            this.textBoxRentId = new System.Windows.Forms.TextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panelAddRentButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.groupBoxButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer.Panel1.Controls.Add(this.panelAddRentButtons);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer.Panel2.Controls.Add(this.pictureBoxLogo);
            this.splitContainer.Panel2.Controls.Add(this.groupBoxButtons);
            this.splitContainer.Size = new System.Drawing.Size(712, 454);
            this.splitContainer.SplitterDistance = 237;
            this.splitContainer.TabIndex = 0;
            // 
            // panelAddRentButtons
            // 
            this.panelAddRentButtons.Controls.Add(this.labelRentId);
            this.panelAddRentButtons.Controls.Add(this.labelDuration);
            this.panelAddRentButtons.Controls.Add(this.textBoxRentId);
            this.panelAddRentButtons.Controls.Add(this.labelComp);
            this.panelAddRentButtons.Controls.Add(this.labelClient);
            this.panelAddRentButtons.Controls.Add(this.maskedTextBoxDuration);
            this.panelAddRentButtons.Controls.Add(this.comboBoxComp);
            this.panelAddRentButtons.Controls.Add(this.comboBoxClient);
            this.panelAddRentButtons.Location = new System.Drawing.Point(3, 3);
            this.panelAddRentButtons.Name = "panelAddRentButtons";
            this.panelAddRentButtons.Size = new System.Drawing.Size(231, 448);
            this.panelAddRentButtons.TabIndex = 0;
            // 
            // labelDuration
            // 
            this.labelDuration.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDuration.AutoSize = true;
            this.labelDuration.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDuration.Location = new System.Drawing.Point(3, 237);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(105, 20);
            this.labelDuration.TabIndex = 7;
            this.labelDuration.Text = "Длительность";
            // 
            // labelComp
            // 
            this.labelComp.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelComp.AutoSize = true;
            this.labelComp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelComp.Location = new System.Drawing.Point(2, 159);
            this.labelComp.Name = "labelComp";
            this.labelComp.Size = new System.Drawing.Size(90, 20);
            this.labelComp.TabIndex = 5;
            this.labelComp.Text = "Компьютер";
            // 
            // labelClient
            // 
            this.labelClient.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelClient.AutoSize = true;
            this.labelClient.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClient.Location = new System.Drawing.Point(3, 84);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(58, 20);
            this.labelClient.TabIndex = 4;
            this.labelClient.Text = "Клиент";
            // 
            // maskedTextBoxDuration
            // 
            this.maskedTextBoxDuration.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.maskedTextBoxDuration.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBoxDuration.Location = new System.Drawing.Point(3, 261);
            this.maskedTextBoxDuration.Mask = "00:00";
            this.maskedTextBoxDuration.Name = "maskedTextBoxDuration";
            this.maskedTextBoxDuration.Size = new System.Drawing.Size(213, 29);
            this.maskedTextBoxDuration.TabIndex = 3;
            this.maskedTextBoxDuration.ValidatingType = typeof(System.DateTime);
            this.maskedTextBoxDuration.MouseClick += new System.Windows.Forms.MouseEventHandler(this.maskedTextBoxDuration_MouseClick);
            // 
            // comboBoxComp
            // 
            this.comboBoxComp.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxComp.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxComp.FormattingEnabled = true;
            this.comboBoxComp.Location = new System.Drawing.Point(3, 183);
            this.comboBoxComp.Name = "comboBoxComp";
            this.comboBoxComp.Size = new System.Drawing.Size(213, 29);
            this.comboBoxComp.TabIndex = 2;
            // 
            // comboBoxClient
            // 
            this.comboBoxClient.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxClient.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxClient.FormattingEnabled = true;
            this.comboBoxClient.Location = new System.Drawing.Point(3, 107);
            this.comboBoxClient.Name = "comboBoxClient";
            this.comboBoxClient.Size = new System.Drawing.Size(213, 29);
            this.comboBoxClient.TabIndex = 1;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(-1, 0);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(473, 390);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 70;
            this.pictureBoxLogo.TabStop = false;
            // 
            // groupBoxButtons
            // 
            this.groupBoxButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxButtons.Controls.Add(this.btnCancel);
            this.groupBoxButtons.Controls.Add(this.btnAddRent);
            this.groupBoxButtons.Location = new System.Drawing.Point(4, 384);
            this.groupBoxButtons.Name = "groupBoxButtons";
            this.groupBoxButtons.Size = new System.Drawing.Size(467, 67);
            this.groupBoxButtons.TabIndex = 69;
            this.groupBoxButtons.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(326, 18);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 35);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddRent
            // 
            this.btnAddRent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddRent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnAddRent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddRent.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAddRent.Location = new System.Drawing.Point(180, 18);
            this.btnAddRent.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddRent.Name = "btnAddRent";
            this.btnAddRent.Size = new System.Drawing.Size(125, 34);
            this.btnAddRent.TabIndex = 4;
            this.btnAddRent.Text = "Добавить";
            this.btnAddRent.UseVisualStyleBackColor = false;
            this.btnAddRent.Click += new System.EventHandler(this.btnAddRent_Click);
            // 
            // labelRentId
            // 
            this.labelRentId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRentId.AutoSize = true;
            this.labelRentId.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRentId.Location = new System.Drawing.Point(3, 12);
            this.labelRentId.Name = "labelRentId";
            this.labelRentId.Size = new System.Drawing.Size(114, 20);
            this.labelRentId.TabIndex = 11;
            this.labelRentId.Text = "Номер аренды";
            // 
            // textBoxRentId
            // 
            this.textBoxRentId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxRentId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxRentId.Location = new System.Drawing.Point(3, 35);
            this.textBoxRentId.Name = "textBoxRentId";
            this.textBoxRentId.Size = new System.Drawing.Size(213, 29);
            this.textBoxRentId.TabIndex = 1;
            this.textBoxRentId.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxRentId_Validating);
            this.textBoxRentId.Validated += new System.EventHandler(this.textBoxRentId_Validated);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // AddRentForm
            // 
            this.AcceptButton = this.btnAddRent;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(712, 454);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddRentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить аренду";
            this.Load += new System.EventHandler(this.AddRentForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panelAddRentButtons.ResumeLayout(false);
            this.panelAddRentButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.groupBoxButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panelAddRentButtons;
        private System.Windows.Forms.ComboBox comboBoxComp;
        private System.Windows.Forms.ComboBox comboBoxClient;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.Label labelComp;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxDuration;
        private System.Windows.Forms.GroupBox groupBoxButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddRent;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelRentId;
        private System.Windows.Forms.TextBox textBoxRentId;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}