﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach.AdminForms.Rents
{
    public partial class AddRentForm : Form
    {
        private short rentId;
        private short client;
        private short computer;
        private short employee;
        private DateTime startTime;
        private DateTime endTime;
        private TimeSpan duration;
        private Employee admin;
        public AddRentForm(Employee admin)
        {
            InitializeComponent();
            this.admin = admin;
        }

        private void AddRent()
        {
            
                using (var db = new ComputerClubModel())
                {

                    client = db.Clients.Where(cl => (cl.Surname + " " + cl.Name + " " + cl.Patronymic).Trim() ==
                        comboBoxClient.SelectedItem.ToString()).First().Client_ID;
                    employee = admin.Employee_ID;
                    computer = (short)comboBoxComp.SelectedItem;
                    startTime = DateTime.Now;
                    duration = TimeSpan.Parse(maskedTextBoxDuration.Text);
                    endTime = startTime + duration;

                    db.Rents.Add(new Rent
                    {
                        Rent_ID = rentId,
                        Client = client,
                        Employee = employee,
                        Start_date_and_time = startTime,
                        Duration = duration
                    });
                    db.SaveChanges();
                    db.Rented_computers.Add(new Rented_computer
                    {
                        Computer = computer,
                        Rent = rentId,
                        Start_date_and_time = startTime,
                        End_date_and_time = endTime
                    });
                    db.SaveChanges();                                
                }
                MessageBox.Show("Данные добавлены!", "Добавлено", MessageBoxButtons.OK);
           
                       
        }

        private void AddRentForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                List<Client> clients = new List<Client>();
                clients.AddRange(db.Clients);
                foreach (var client in clients)
                {
                    comboBoxClient.Items.Add(client.Surname + " " + client.Name + " " + client.Patronymic);
                }
                List<Computer> computers = new List<Computer>();
                computers.AddRange(db.Computers);
                foreach(var computer in computers)
                {
                    comboBoxComp.Items.Add(computer.Computer_ID);
                }
            }
        }

        private void btnAddRent_Click(object sender, EventArgs e)
        {
            AddRent();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Close();
            AdminForm adminForm = new AdminForm(admin);
            adminForm.ShowDialog();
        }

        private void maskedTextBoxDuration_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxDuration.SelectionStart = 0;
        }
        private void textBoxRentId_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxRentId.Text.Trim();
            using (var db = new ComputerClubModel())
            {
                if (db.Rents.Where(cl => cl.Rent_ID.ToString() == input).Any())
                {
                    errorProvider.SetError(textBoxRentId, "Аренда с таким номером уже существует!");
                    e.Cancel = true;
                }
                else if (Regex.IsMatch(input, @"(?<=\s|^)\d+(?=\s|$)"))
                {
                    errorProvider.SetError(textBoxRentId, String.Empty);
                    e.Cancel = false;
                }
                else
                {
                    errorProvider.SetError(textBoxRentId, "Ошибка!");
                    e.Cancel = true;
                }
            }
        }
        private void textBoxRentId_Validated(object sender, EventArgs e)
        {
            rentId = (short)Convert.ToInt32(textBoxRentId.Text.Trim());
        }     
    }
}
