﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.Rents
{
    public partial class CurrentRentsForm : Form
    {
        private Employee admin;
        public CurrentRentsForm(Employee admin)
        {
            InitializeComponent();
            this.admin = admin;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminForm adminForm = new AdminForm(admin);
            adminForm.ShowDialog();
        }

        private void SetRentsGrid()
        {
            RentsGridView.DataSource = GetCurrentRents();
            RentsGridView.Columns[0].HeaderText = "Номер аренды";
            RentsGridView.Columns[1].HeaderText = "Клиент";
            RentsGridView.Columns[2].HeaderText = "Компьютер";
            RentsGridView.Columns[3].HeaderText = "Сотрудник";
            RentsGridView.Columns[4].HeaderText = "Время начала";
            RentsGridView.Columns[5].HeaderText = "Длительность";
            RentsGridView.Columns[6].HeaderText = "Время окончания";
        }

        private IEnumerable GetCurrentRents()
        {
            using (var db = new ComputerClubModel())
            {
                var rents = from rent in db.Rents.Join(db.Rented_computers,
                    r => r.Rent_ID,
                    c => c.Rent,
                    (r, c) => new
                    {
                        r.Rent_ID,
                        r.Client,
                        c.Computer,
                        r.Employee,
                        r.Start_date_and_time,
                        r.Duration,
                        c.End_date_and_time
                    })
                            select new
                            {
                                rent.Rent_ID,
                                rent.Client,
                                rent.Computer,
                                rent.Employee,
                                rent.Start_date_and_time,
                                rent.Duration,
                                rent.End_date_and_time
                            };
               
                return rents.Where(r => r.Start_date_and_time <= DateTime.Now &&
                r.End_date_and_time >= DateTime.Now).ToList();
            }
        }

       
        private void CurrentRentsForm_Load(object sender, EventArgs e)
        {
            SetRentsGrid();
        }

        private void addRentMenuItem_Click(object sender, EventArgs e)
        {
            AddRentForm addRentForm = new AddRentForm(admin);
            addRentForm.ShowDialog();
        }

        private void updateMenuItem_Click(object sender, EventArgs e)
        {
            if (RentsGridView.SelectedCells.Count > 0)
            {
                var i = RentsGridView.SelectedCells[0].OwningRow.Index;
                short rentId = (short)RentsGridView[0, i].Value;
                UpdateRentForm update = new UpdateRentForm(rentId, admin);
                if (update.ShowDialog(this) == DialogResult.OK)
                {
                    SetRentsGrid(); 
                }
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите удалить этот элемент?",
                "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                if (RentsGridView.SelectedCells.Count > 0)
                {
                    var i = RentsGridView.SelectedCells[0].OwningRow.Index;
                    short rentId = (short)RentsGridView[0, i].Value;
                    using (var db = new ComputerClubModel())
                    {
                        Rent rent = db.Rents.Where(r => r.Rent_ID == rentId).First();
                        Rented_computer rentedComp = db.Rented_computers.Where(rc => rc.Rent == rentId).First();
                        db.Rented_computers.Remove(rentedComp);
                        db.Rents.Remove(rent);
                        db.SaveChanges();
                    }
                }
            }
            SetRentsGrid();
        }

        private void refreshMenuItem_Click(object sender, EventArgs e)
        {
            SetRentsGrid();
        }

        private void buttonAllRents_Click(object sender, EventArgs e)
        {
            RentsHistoryForm rentsHistoryForm = new RentsHistoryForm(admin);
            this.Hide();
            rentsHistoryForm.ShowDialog();
        }

        private void showCheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                var i = RentsGridView.SelectedCells[0].OwningRow.Index;
                short rentId = (short)RentsGridView[0, i].Value;
                Rent rent = db.Rents.Where(r => r.Rent_ID == rentId).First();
                CheckForm checkForm = new CheckForm(rent);
                checkForm.ShowDialog();
            }
                
        }
    }
}
