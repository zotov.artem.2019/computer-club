﻿namespace Kursach.AdminForms.Rents
{
    partial class CheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckForm));
            this.labelRent = new System.Windows.Forms.Label();
            this.labelClient = new System.Windows.Forms.Label();
            this.labelComp = new System.Windows.Forms.Label();
            this.labelEmployee = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelCost = new System.Windows.Forms.Label();
            this.labelCostWithCard = new System.Windows.Forms.Label();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelRent
            // 
            this.labelRent.AutoSize = true;
            this.labelRent.Location = new System.Drawing.Point(35, 24);
            this.labelRent.Name = "labelRent";
            this.labelRent.Size = new System.Drawing.Size(53, 15);
            this.labelRent.TabIndex = 0;
            this.labelRent.Text = "Аренда: ";
            // 
            // labelClient
            // 
            this.labelClient.AutoSize = true;
            this.labelClient.Location = new System.Drawing.Point(35, 50);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(49, 15);
            this.labelClient.TabIndex = 1;
            this.labelClient.Text = "Клиент:";
            // 
            // labelComp
            // 
            this.labelComp.AutoSize = true;
            this.labelComp.Location = new System.Drawing.Point(35, 78);
            this.labelComp.Name = "labelComp";
            this.labelComp.Size = new System.Drawing.Size(74, 15);
            this.labelComp.TabIndex = 2;
            this.labelComp.Text = "Компьютер:";
            // 
            // labelEmployee
            // 
            this.labelEmployee.AutoSize = true;
            this.labelEmployee.Location = new System.Drawing.Point(35, 107);
            this.labelEmployee.Name = "labelEmployee";
            this.labelEmployee.Size = new System.Drawing.Size(69, 15);
            this.labelEmployee.TabIndex = 3;
            this.labelEmployee.Text = "Сотрудник:";
            // 
            // labelDuration
            // 
            this.labelDuration.AutoSize = true;
            this.labelDuration.Location = new System.Drawing.Point(35, 136);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(87, 15);
            this.labelDuration.TabIndex = 4;
            this.labelDuration.Text = "Длительность:";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(35, 166);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(35, 15);
            this.labelDate.TabIndex = 5;
            this.labelDate.Text = "Дата:";
            // 
            // labelCost
            // 
            this.labelCost.AutoSize = true;
            this.labelCost.Location = new System.Drawing.Point(35, 196);
            this.labelCost.Name = "labelCost";
            this.labelCost.Size = new System.Drawing.Size(58, 15);
            this.labelCost.TabIndex = 6;
            this.labelCost.Text = "К оплате:";
            // 
            // labelCostWithCard
            // 
            this.labelCostWithCard.AutoSize = true;
            this.labelCostWithCard.Location = new System.Drawing.Point(35, 225);
            this.labelCostWithCard.Name = "labelCostWithCard";
            this.labelCostWithCard.Size = new System.Drawing.Size(99, 15);
            this.labelCostWithCard.TabIndex = 7;
            this.labelCostWithCard.Text = "Скидка по карте:";
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Location = new System.Drawing.Point(35, 254);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(43, 15);
            this.labelTotalPrice.TabIndex = 8;
            this.labelTotalPrice.Text = "Итого:";
            // 
            // CheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(297, 330);
            this.Controls.Add(this.labelTotalPrice);
            this.Controls.Add(this.labelCostWithCard);
            this.Controls.Add(this.labelCost);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelDuration);
            this.Controls.Add(this.labelEmployee);
            this.Controls.Add(this.labelComp);
            this.Controls.Add(this.labelClient);
            this.Controls.Add(this.labelRent);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "CheckForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Чек";
            this.Load += new System.EventHandler(this.CheckForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRent;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.Label labelComp;
        private System.Windows.Forms.Label labelEmployee;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelCost;
        private System.Windows.Forms.Label labelCostWithCard;
        private System.Windows.Forms.Label labelTotalPrice;
    }
}