﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.Rents
{
    public partial class RentsHistoryForm : Form
    {
        private Employee admin;
        public RentsHistoryForm(Employee admin)
        {
            InitializeComponent();
            this.admin = admin;
        }

        private void SetAllRentsGrid()
        {
            AllRentsGridView.DataSource = GetAllRents();
            AllRentsGridView.Columns[0].HeaderText = "Номер аренды";
            AllRentsGridView.Columns[1].HeaderText = "Клиент";
            AllRentsGridView.Columns[2].HeaderText = "Компьютер";
            AllRentsGridView.Columns[3].HeaderText = "Сотрудник";
            AllRentsGridView.Columns[4].HeaderText = "Время начала";
            AllRentsGridView.Columns[5].HeaderText = "Длительность";
            AllRentsGridView.Columns[6].HeaderText = "Время окончания";
        }
        private IEnumerable GetAllRents()
        {
            using (var db = new ComputerClubModel())
            {
                var rents = from rent in db.Rents.Join(db.Rented_computers,
                    r => r.Rent_ID,
                    c => c.Rent,
                    (r, c) => new
                    {
                        r.Rent_ID,
                        r.Client,
                        c.Computer,
                        r.Employee,
                        r.Start_date_and_time,
                        r.Duration,
                        c.End_date_and_time
                    })
                            select new
                            {
                                rent.Rent_ID,
                                rent.Client,
                                rent.Computer,
                                rent.Employee,
                                rent.Start_date_and_time,
                                rent.Duration,
                                rent.End_date_and_time
                            };

                return rents.ToList();
            }
        }
        private void RentsHistoryForm_Load(object sender, EventArgs e)
        {
            SetAllRentsGrid();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            CurrentRentsForm currentRentsForm = new CurrentRentsForm(admin);
            currentRentsForm.ShowDialog();
        }

        private void RentsHistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
