﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach.AdminForms.Rents
{
    public partial class CheckForm : Form
    {   
        private Rent rent;
        public CheckForm(Rent rent)
        {
            InitializeComponent();
            this.rent = rent;
        }

        private void CheckForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                short computer = db.Rented_computers.Where(c => c.Rent == rent.Rent_ID).First().Computer;
                short clientId = rent.Client;
    
                labelRent.Text = labelRent.Text.Trim() + " " + rent.Rent_ID;
                labelClient.Text = labelClient.Text.Trim() + " " + db.Clients.Where(cl => cl.Client_ID == rent.Client).First().Surname + " " +
                    db.Clients.Where(cl => cl.Client_ID == rent.Client).First().Name + " " + db.Clients.Where(cl => cl.Client_ID == rent.Client).First().Patronymic;
                labelDuration.Text = labelDuration.Text.Trim();
                labelComp.Text = labelComp.Text.Trim() + " " + computer;
                labelEmployee.Text = labelEmployee.Text.Trim() + " " + db.Employees.Where(empl => empl.Employee_ID == rent.Employee).First().Surname + " " +
                    db.Employees.Where(empl => empl.Employee_ID == rent.Employee).First().Name + " " + db.Employees.Where(empl => empl.Employee_ID == rent.Employee).First().Patronymic;
                labelDuration.Text = labelDuration.Text.Trim() + " " + rent.Duration;
                labelDate.Text = labelDate.Text.Trim() + " " + rent.Start_date_and_time.ToShortDateString();
                decimal cost = rent.Duration.Hours * db.Computer_types.Where(type => type.Type_ID ==
                db.Computers.Where(c => c.Computer_ID == computer).FirstOrDefault().Type).FirstOrDefault().Cost_per_hour;
                labelCost.Text = labelCost.Text.Trim() + " " + cost;
                decimal costWithCard;
                if (db.Club_cards.Where(c => c.Client == clientId).Any())
                {
                    costWithCard = cost * ParseDiscount(db.Club_cards.Where(c => c.Client == clientId).First());
                }
                else
                {
                    costWithCard = 0;
                }
                
                labelCostWithCard.Text = labelCostWithCard.Text.Trim() + " " + costWithCard;
                labelTotalPrice.Text = labelTotalPrice.Text.Trim() + " " + (cost - costWithCard).ToString();
            }             
        }

        private decimal ParseDiscount(Club_card card)
        {
            using (var db = new ComputerClubModel())
            {
                if (card == null)
                {
                    return 0;
                }
                else
                {
                    byte cardType = card.Type;
                    double discount = db.Card_types.Where(type => type.Type_ID == cardType).FirstOrDefault().Discount;
                    decimal result = (decimal) (discount * 0.01);
                    return result;
                }
               
            }
        }      
    }
}
