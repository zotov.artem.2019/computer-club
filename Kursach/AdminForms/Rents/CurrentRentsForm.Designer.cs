﻿namespace Kursach.AdminForms.Rents
{
    partial class CurrentRentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CurrentRentsForm));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.buttonAllRents = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.RentsGridView = new System.Windows.Forms.DataGridView();
            this.RentContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addRentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отобразитьЧекToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RentsGridView)).BeginInit();
            this.RentContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer.Location = new System.Drawing.Point(1, 1);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer.Panel1.Controls.Add(this.buttonAllRents);
            this.splitContainer.Panel1.Controls.Add(this.btnBack);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.RentsGridView);
            this.splitContainer.Size = new System.Drawing.Size(1092, 636);
            this.splitContainer.SplitterDistance = 154;
            this.splitContainer.TabIndex = 43;
            // 
            // buttonAllRents
            // 
            this.buttonAllRents.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonAllRents.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAllRents.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAllRents.Location = new System.Drawing.Point(2, 62);
            this.buttonAllRents.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAllRents.Name = "buttonAllRents";
            this.buttonAllRents.Size = new System.Drawing.Size(146, 47);
            this.buttonAllRents.TabIndex = 2;
            this.buttonAllRents.Text = "Все аренды";
            this.buttonAllRents.UseVisualStyleBackColor = false;
            this.buttonAllRents.Click += new System.EventHandler(this.buttonAllRents_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBack.Location = new System.Drawing.Point(2, 2);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(146, 47);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Назад";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // RentsGridView
            // 
            this.RentsGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RentsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RentsGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.RentsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RentsGridView.ContextMenuStrip = this.RentContextMenu;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RentsGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.RentsGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.RentsGridView.Location = new System.Drawing.Point(-4, -2);
            this.RentsGridView.Name = "RentsGridView";
            this.RentsGridView.Size = new System.Drawing.Size(937, 634);
            this.RentsGridView.TabIndex = 2;
            // 
            // RentContextMenu
            // 
            this.RentContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRentMenuItem,
            this.updateMenuItem,
            this.deleteMenuItem,
            this.refreshMenuItem,
            this.отобразитьЧекToolStripMenuItem});
            this.RentContextMenu.Name = "ClientContextMenu";
            this.RentContextMenu.Size = new System.Drawing.Size(181, 136);
            // 
            // addRentMenuItem
            // 
            this.addRentMenuItem.Name = "addRentMenuItem";
            this.addRentMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addRentMenuItem.Text = "Добавить";
            this.addRentMenuItem.Click += new System.EventHandler(this.addRentMenuItem_Click);
            // 
            // updateMenuItem
            // 
            this.updateMenuItem.Name = "updateMenuItem";
            this.updateMenuItem.Size = new System.Drawing.Size(180, 22);
            this.updateMenuItem.Text = "Изменить";
            this.updateMenuItem.Click += new System.EventHandler(this.updateMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteMenuItem.Text = "Удалить";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // refreshMenuItem
            // 
            this.refreshMenuItem.Name = "refreshMenuItem";
            this.refreshMenuItem.Size = new System.Drawing.Size(180, 22);
            this.refreshMenuItem.Text = "Обновить";
            this.refreshMenuItem.Click += new System.EventHandler(this.refreshMenuItem_Click);
            // 
            // отобразитьЧекToolStripMenuItem
            // 
            this.отобразитьЧекToolStripMenuItem.Name = "отобразитьЧекToolStripMenuItem";
            this.отобразитьЧекToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.отобразитьЧекToolStripMenuItem.Text = "Отобразить чек";
            this.отобразитьЧекToolStripMenuItem.Click += new System.EventHandler(this.showCheckToolStripMenuItem_Click);
            // 
            // CurrentRentsForm
            // 
            this.AcceptButton = this.buttonAllRents;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBack;
            this.ClientSize = new System.Drawing.Size(1092, 636);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CurrentRentsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Текущие аренды";
            this.Load += new System.EventHandler(this.CurrentRentsForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RentsGridView)).EndInit();
            this.RentContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.DataGridView RentsGridView;
        private System.Windows.Forms.ContextMenuStrip RentContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addRentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshMenuItem;
        private System.Windows.Forms.Button buttonAllRents;
        private System.Windows.Forms.ToolStripMenuItem отобразитьЧекToolStripMenuItem;
    }
}