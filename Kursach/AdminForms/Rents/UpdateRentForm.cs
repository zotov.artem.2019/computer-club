﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach.AdminForms.Rents
{
    public partial class UpdateRentForm : Form
    {
        private short rentId;
        private short client;
        private short computer;
        private short employee;
        private DateTime startTime;
        private DateTime endTime;
        private TimeSpan duration;
        private Employee admin;
        public UpdateRentForm(short rentId, Employee admin)
        {
            InitializeComponent();
            this.rentId = rentId;
            this.admin = admin;
        }

        private void UpdateRentForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                textBoxRentId.Text = rentId.ToString();
                List<Client> clients = new List<Client>();
                clients.AddRange(db.Clients);
                foreach (var client in clients)
                {
                    comboBoxClient.Items.Add(client.Surname + " " + client.Name + " " + client.Patronymic);
                }
                List<Computer> computers = new List<Computer>();
                computers.AddRange(db.Computers);
                foreach (var computer in computers)
                {
                    comboBoxComp.Items.Add(computer.Computer_ID);
                }

                Rent rent = db.Rents.Where(r => r.Rent_ID == rentId).First();
                Rented_computer rentedComp = db.Rented_computers.Where(rc => rc.Rent == rentId).First();
                comboBoxClient.Text = db.Clients.Where(cl => cl.Client_ID == rent.Client).First().Surname + " " +
                    db.Clients.Where(cl => cl.Client_ID == rent.Client).First().Name + " " +
                    db.Clients.Where(cl => cl.Client_ID == rent.Client).First().Patronymic;
                maskedTextBoxDuration.Text = rent.Duration.ToString();
                comboBoxComp.Text = rentedComp.Computer.ToString();
            }
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите изменить этот элемент?",
                "Изменение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            DialogResult = ValidateChildren() ? DialogResult.OK : DialogResult.None;
            if (DialogResult == DialogResult.OK)
            {
                UpdateRent();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            Close();
        }

        private void UpdateRent()
        {
            try
            {
                using (var db = new ComputerClubModel())
                {

                    client = db.Clients.Where(cl => (cl.Surname + " " + cl.Name + " " + cl.Patronymic).Trim() ==
                        comboBoxClient.SelectedItem.ToString()).First().Client_ID;
                    employee = admin.Employee_ID;
                    computer = (short)comboBoxComp.SelectedItem;
                    startTime = DateTime.Now;
                    duration = TimeSpan.Parse(maskedTextBoxDuration.Text);
                    endTime = startTime + duration;

                    Rent rent = db.Rents.Where(r => r.Rent_ID == rentId).First();
                    Rented_computer rentedComp = db.Rented_computers.Where(rc => rc.Rent == rentId).First();
                    db.Rents.Remove(rent);
                    db.SaveChanges();
                    db.Rents.Add(new Rent
                    {
                        Rent_ID = rentId,
                        Client = client,
                        Employee = employee,
                        Start_date_and_time = startTime,
                        Duration = duration
                    });
                    db.Rented_computers.Add(new Rented_computer
                    {
                        Computer = computer,
                        Rent = rentId,
                        Start_date_and_time = startTime,
                        End_date_and_time = endTime
                    });
                    db.SaveChanges();
                    
                }
                MessageBox.Show("Данные добавлены!", "Добавлено", MessageBoxButtons.OK);
                this.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка в данных!", "Ошибка", MessageBoxButtons.OK);
            }
        }
    }
}
