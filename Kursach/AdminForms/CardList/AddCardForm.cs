﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Kursach.AdminForms.CardList
{
    public partial class AddCardForm : Form
    {
        private short cardId;
        private byte type;
        private short clientId;
        private short? validity;
        private DateTime dateOfIssue;
        private EntityManager manager = new EntityManager();

        public AddCardForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddCard();
            this.Close();
        }
        private void AddCard()
        {
            try
            {           
                using (var db = new ComputerClubModel())
                {
                    type = db.Card_types.Where(
                        card => card.Name == comboBoxType.SelectedItem.ToString()).First().Type_ID;
                    clientId = db.Clients.Where(
                        client => (client.Surname + " " + client.Name + " " + client.Patronymic).Trim() == 
                        comboBoxClient.SelectedItem.ToString()).First().Client_ID;
                    dateOfIssue = dateOfIssuePicker.Value;
                    validity = manager.ParseValidity(comboBoxValidity.SelectedItem.ToString());
                    
                    db.Club_cards.Add(new Club_card
                    {
                        Card_ID = cardId,
                        Type = type,
                        Client = clientId,
                        Validity = validity,
                        Date_of_issue = dateOfIssue
                    });
                    db.SaveChanges();
                }
                MessageBox.Show("Данные добавлены!", "Добавлено", MessageBoxButtons.OK);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка в данных!", "Ошибка", MessageBoxButtons.OK);
            }
        }
      

        private void AddCardForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                List<Client> clients = new List<Client>();
                clients.AddRange(db.Clients);
                foreach (var client in clients)
                {
                    comboBoxClient.Items.Add(client.Surname + " " + client.Name + " " + client.Patronymic);
                }
                List<Card_type> types = new List<Card_type>();
                types.AddRange(db.Card_types);
                foreach (var type in types)
                {
                    comboBoxType.Items.Add(type.Name);
                }
                comboBoxValidity.Items.Add("1 год");
                comboBoxValidity.Items.Add("2 года");
                comboBoxValidity.Items.Add("3 года");
                comboBoxValidity.Items.Add("4 года");
                comboBoxValidity.Items.Add("5 лет");
                comboBoxValidity.Items.Add("бессрочная");
            }
        }

        private void textBoxCardId_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxCardId.Text.Trim();
            using (var db = new ComputerClubModel())
            {
                if (db.Club_cards.Where(cl => cl.Card_ID.ToString() == input).Any())
                {
                    errorProvider.SetError(textBoxCardId, "Карта с таким номером уже существует!");
                    e.Cancel = true;
                }
                else if (Regex.IsMatch(input, @"(?<=\s|^)\d+(?=\s|$)"))
                {
                    errorProvider.SetError(textBoxCardId, String.Empty);
                    e.Cancel = false;
                }
                else
                {
                    errorProvider.SetError(textBoxCardId, "Ошибка!");
                    e.Cancel = true;
                }
            }
        }

        private void textBoxCardId_Validated(object sender, EventArgs e)
        {
            cardId = (short)Convert.ToInt32(textBoxCardId.Text.Trim());
        }
    }
}
