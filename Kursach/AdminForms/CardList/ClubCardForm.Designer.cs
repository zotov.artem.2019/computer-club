﻿namespace Kursach.AdminForms.CardList
{
    partial class ClubCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClubCardForm));
            this.splitContainerCard = new System.Windows.Forms.SplitContainer();
            this.btnBack = new System.Windows.Forms.Button();
            this.CardsGridView = new System.Windows.Forms.DataGridView();
            this.CardContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerCard)).BeginInit();
            this.splitContainerCard.Panel1.SuspendLayout();
            this.splitContainerCard.Panel2.SuspendLayout();
            this.splitContainerCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CardsGridView)).BeginInit();
            this.CardContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerCard
            // 
            this.splitContainerCard.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainerCard.Location = new System.Drawing.Point(0, 0);
            this.splitContainerCard.Name = "splitContainerCard";
            // 
            // splitContainerCard.Panel1
            // 
            this.splitContainerCard.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainerCard.Panel1.Controls.Add(this.btnBack);
            // 
            // splitContainerCard.Panel2
            // 
            this.splitContainerCard.Panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainerCard.Panel2.Controls.Add(this.CardsGridView);
            this.splitContainerCard.Size = new System.Drawing.Size(1091, 636);
            this.splitContainerCard.SplitterDistance = 154;
            this.splitContainerCard.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBack.Location = new System.Drawing.Point(2, 2);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(146, 47);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Назад";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // CardsGridView
            // 
            this.CardsGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CardsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CardsGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.CardsGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CardsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CardsGridView.ContextMenuStrip = this.CardContextMenu;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CardsGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.CardsGridView.Location = new System.Drawing.Point(0, 0);
            this.CardsGridView.Name = "CardsGridView";
            this.CardsGridView.Size = new System.Drawing.Size(928, 632);
            this.CardsGridView.TabIndex = 1;
            // 
            // CardContextMenu
            // 
            this.CardContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMenuItem,
            this.updateMenuItem,
            this.deleteMenuItem,
            this.refreshMenuItem});
            this.CardContextMenu.Name = "ClientContextMenu";
            this.CardContextMenu.Size = new System.Drawing.Size(129, 92);
            // 
            // addMenuItem
            // 
            this.addMenuItem.Name = "addMenuItem";
            this.addMenuItem.Size = new System.Drawing.Size(128, 22);
            this.addMenuItem.Text = "Добавить";
            this.addMenuItem.Click += new System.EventHandler(this.addMenuItem_Click);
            // 
            // updateMenuItem
            // 
            this.updateMenuItem.Name = "updateMenuItem";
            this.updateMenuItem.Size = new System.Drawing.Size(128, 22);
            this.updateMenuItem.Text = "Изменить";
            this.updateMenuItem.Click += new System.EventHandler(this.updateMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(128, 22);
            this.deleteMenuItem.Text = "Удалить";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // refreshMenuItem
            // 
            this.refreshMenuItem.Name = "refreshMenuItem";
            this.refreshMenuItem.Size = new System.Drawing.Size(128, 22);
            this.refreshMenuItem.Text = "Обновить";
            this.refreshMenuItem.Click += new System.EventHandler(this.refreshMenuItem_Click);
            // 
            // ClubCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBack;
            this.ClientSize = new System.Drawing.Size(1092, 636);
            this.Controls.Add(this.splitContainerCard);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ClubCardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Клубные карты";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClubCardForm_FormClosing);
            this.Load += new System.EventHandler(this.ClubCardForm_Load);
            this.splitContainerCard.Panel1.ResumeLayout(false);
            this.splitContainerCard.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerCard)).EndInit();
            this.splitContainerCard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CardsGridView)).EndInit();
            this.CardContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerCard;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.DataGridView CardsGridView;
        private System.Windows.Forms.ContextMenuStrip CardContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshMenuItem;
    }
}