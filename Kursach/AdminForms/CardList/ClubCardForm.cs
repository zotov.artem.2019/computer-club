﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.CardList
{
    public partial class ClubCardForm : Form
    {
        private Employee admin;
        private EntityManager manager = new EntityManager();
        public ClubCardForm(Employee admin)
        {
            InitializeComponent();
            this.admin = admin;
        }


        private void SetCardsGrid()
        {
            CardsGridView.DataSource = manager.GetCards();
            CardsGridView.Columns[0].HeaderText = "Номер карты";
            CardsGridView.Columns[1].HeaderText = "Тип";
            CardsGridView.Columns[2].HeaderText = "Клиент";
            CardsGridView.Columns[3].HeaderText = "Дата оформления";
            CardsGridView.Columns[4].HeaderText = "Срок действия";
        }

        private void ClubCardForm_Load(object sender, EventArgs e)
        {
            SetCardsGrid();
        }

        private void addMenuItem_Click(object sender, EventArgs e)
        {
            AddCardForm add = new AddCardForm();
            if (add.ShowDialog(this) == DialogResult.OK)
            {
                SetCardsGrid();
            }
        }

        private void updateMenuItem_Click(object sender, EventArgs e)
        {
            if (CardsGridView.SelectedCells.Count > 0)
            {
                var i = CardsGridView.SelectedCells[0].OwningRow.Index;
                short cardID = (short)CardsGridView[0, i].Value;
                UpdateCardForm update = new UpdateCardForm(cardID);
                if (update.ShowDialog(this) == DialogResult.OK)
                {
                    SetCardsGrid();
                }
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите удалить этот элемент?",
               "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                if (CardsGridView.SelectedCells.Count > 0)
                {
                    var i = CardsGridView.SelectedCells[0].OwningRow.Index;
                    short cardID = (short)CardsGridView[0, i].Value;
                    using (var db = new ComputerClubModel())
                    {
                        Club_card card = db.Club_cards.Where(x => x.Card_ID == cardID).First();
                        db.Club_cards.Remove(card);
                        db.SaveChanges();
                    }
                }
            }
            SetCardsGrid();
        }

        private void refreshMenuItem_Click(object sender, EventArgs e)
        {
            SetCardsGrid();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminForm adminForm = new AdminForm(admin);
            adminForm.ShowDialog();
        }

        private void ClubCardForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
