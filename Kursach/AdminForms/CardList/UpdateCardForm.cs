﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach.AdminForms.CardList
{
    public partial class UpdateCardForm : Form
    {
        private short cardId;
        private byte type;
        private short clientId;
        private short? validity;
        private DateTime dateOfIssue;
        private EntityManager manager = new EntityManager();
        public UpdateCardForm(short cardIdOuter)
        {
            InitializeComponent();
            this.cardId = cardIdOuter;
        }

        private void UpdateCard()
        {

            using (var db = new ComputerClubModel())
            {
                try
                {
                    type = db.Card_types.Where(
                                          c => c.Name == comboBoxType.SelectedItem.ToString()).First().Type_ID;
                    clientId = db.Clients.Where(
                        client => (client.Surname + " " + client.Name + " " + client.Patronymic).Trim() ==
                        comboBoxClient.SelectedItem.ToString()).First().Client_ID;
                    dateOfIssue = dateOfIssuePicker.Value;
                    validity = manager.ParseValidity(comboBoxValidity.SelectedItem.ToString());
                    Club_card card = db.Club_cards.Where(x => x.Card_ID == cardId).First();
                    card.Type = db.Card_types.Where(
                            c => c.Name == comboBoxType.SelectedItem.ToString()).First().Type_ID;
                    card.Client = clientId;
                    card.Validity = validity;
                    card.Date_of_issue = dateOfIssue;
                    db.SaveChanges();
                } catch 
                {
                    MessageBox.Show("Ошибка в данных!");
                }
                
            }
        }

        private void UpdateCardForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {              
                List<Client> clients = new List<Client>();
                clients.AddRange(db.Clients);
                foreach (var client in clients)
                {
                    comboBoxClient.Items.Add(client.Surname + " " + client.Name + " " + client.Patronymic);
                }
                List<Card_type> types = new List<Card_type>();
                types.AddRange(db.Card_types);
                foreach (var type in types)
                {
                    comboBoxType.Items.Add(type.Name);
                }
                comboBoxValidity.Items.Add("1 год");
                comboBoxValidity.Items.Add("2 года");
                comboBoxValidity.Items.Add("3 года");
                comboBoxValidity.Items.Add("4 года");
                comboBoxValidity.Items.Add("5 лет");
                comboBoxValidity.Items.Add("бессрочная");
                textBoxCardId.Text = cardId.ToString();

                Club_card card = db.Club_cards.Where(c => c.Card_ID == cardId).First();
                comboBoxClient.Text = db.Clients.Where(cl => cl.Client_ID == card.Client).First().Surname + " " +
                    db.Clients.Where(cl => cl.Client_ID == card.Client).First().Name + " " +
                    db.Clients.Where(cl => cl.Client_ID == card.Client).First().Patronymic;
                comboBoxValidity.Text = GetValidity(card.Validity);
                comboBoxType.Text = db.Card_types.Where(type => type.Type_ID == card.Type).First().Name;
                dateOfIssuePicker.Value = card.Date_of_issue;

            }
        }

        private string GetValidity(short? validity)
        {
            string result;
            if (validity == 1)
            {
                result = "1 год";
            } else if (validity == 2)
            {
                result = "2 года";
            } else if (validity == 3)
            {
                result = "3 года";
            } else if (validity == 4)
            {
                result = "4 года";
            } else if (validity == 5)
            {
                result = "5 лет";
            } else
            {
                result = "бессрочная";
            }
            return result;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите изменить этот элемент?",
                "Изменение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            DialogResult = ValidateChildren() ? DialogResult.OK : DialogResult.None;
            if (DialogResult == DialogResult.OK)
            {
                UpdateCard();
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            Close();
        }
    }
}
