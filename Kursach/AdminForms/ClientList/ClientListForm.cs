﻿using Kursach.AdminForms;
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach
{
    public partial class ClientListForm : Form
    {
        Employee admin;
        public ClientListForm(Employee admin)
        {
            this.admin = admin;
            InitializeComponent();
        }

        public IEnumerable GetClients()
        {
            using (var db = new ComputerClubModel())
            {
                var clients = from client in db.Clients
                            select new
                            {
                                client.Client_ID,
                                client.Surname,
                                client.Name,
                                client.Patronymic,
                                client.Login,
                                client.Password,
                                client.Passport_Series,
                                client.Passport_ID,
                                client.Address,
                                client.Phone,
                                client.Date_of_birth
                            };
                return clients.ToList();
            }
        }

        private void SetClientsGrid()
        {
            ClientsGridView.DataSource = GetClients();
            ClientsGridView.Columns[0].HeaderText = "Номер клиента";
            ClientsGridView.Columns[1].HeaderText = "Фамилия";
            ClientsGridView.Columns[2].HeaderText = "Имя";
            ClientsGridView.Columns[3].HeaderText = "Отчество";
            ClientsGridView.Columns[4].HeaderText = "Логин";
            ClientsGridView.Columns[5].HeaderText = "Пароль";
            ClientsGridView.Columns[6].HeaderText = "Серия паспорта";
            ClientsGridView.Columns[7].HeaderText = "Номер паспорта";
            ClientsGridView.Columns[8].HeaderText = "Адрес";
            ClientsGridView.Columns[9].HeaderText = "Телефон";
            ClientsGridView.Columns[10].HeaderText = "Дата рождения";
        }

        private void ClientListForm_Load(object sender, EventArgs e)
        {
            SetClientsGrid();
        }

        private void refreshMenuItem_Click(object sender, EventArgs e)
        {
            SetClientsGrid();
        }

        private void addClientMenuItem_Click(object sender, EventArgs e)
        {
            AddClientForm add = new AddClientForm();
            if (add.ShowDialog(this) == DialogResult.OK)
            {
                SetClientsGrid();
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите удалить этот элемент?",
                "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                if (ClientsGridView.SelectedCells.Count > 0)
                {
                    var i = ClientsGridView.SelectedCells[0].OwningRow.Index;
                    short clientID = (short)ClientsGridView[0, i].Value;
                    using (var db = new ComputerClubModel())
                    {
                        Client client = db.Clients.Where(x => x.Client_ID == clientID).First();
                        db.Clients.Remove(client);
                        db.SaveChanges();
                    }
                }
            }
            SetClientsGrid();
        }

        private void updateMenuItem_Click(object sender, EventArgs e)
        {
            if (ClientsGridView.SelectedCells.Count > 0)
            {
                var i = ClientsGridView.SelectedCells[0].OwningRow.Index;
                short clientId = (short)ClientsGridView[0, i].Value;
                UpdateClientForm update = new UpdateClientForm(clientId);
                if (update.ShowDialog(this) == DialogResult.OK)
                {
                    SetClientsGrid();
                }
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminForm adminForm = new AdminForm(admin);
            adminForm.ShowDialog(); 
        }

        private void ClientListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
