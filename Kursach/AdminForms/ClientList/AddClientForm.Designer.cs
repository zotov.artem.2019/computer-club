﻿namespace Kursach.AdminForms
{
    partial class AddClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddClientForm));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.labelPassportId = new System.Windows.Forms.Label();
            this.labelPatronymic = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.labelLogin = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPassportSeries = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxPatronymic = new System.Windows.Forms.TextBox();
            this.labelSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.dateOfBirthPicker = new System.Windows.Forms.DateTimePicker();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.maskedTextBoxPhone = new System.Windows.Forms.MaskedTextBox();
            this.panelAddClient = new System.Windows.Forms.Panel();
            this.textBoxClientId = new System.Windows.Forms.TextBox();
            this.labelId = new System.Windows.Forms.Label();
            this.maskedTextBoxPassword = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxPassportId = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxPassportSeries = new System.Windows.Forms.MaskedTextBox();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.groupBoxButtons = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.panelAddClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.groupBoxButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(325, 18);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 34);
            this.btnCancel.TabIndex = 65;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAdd.Location = new System.Drawing.Point(185, 18);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(125, 34);
            this.btnAdd.TabIndex = 64;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // labelPassportId
            // 
            this.labelPassportId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelPassportId.AutoSize = true;
            this.labelPassportId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassportId.Location = new System.Drawing.Point(3, 521);
            this.labelPassportId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPassportId.Name = "labelPassportId";
            this.labelPassportId.Size = new System.Drawing.Size(128, 21);
            this.labelPassportId.TabIndex = 62;
            this.labelPassportId.Text = "Номер паспорта";
            // 
            // labelPatronymic
            // 
            this.labelPatronymic.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelPatronymic.AutoSize = true;
            this.labelPatronymic.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPatronymic.Location = new System.Drawing.Point(3, 198);
            this.labelPatronymic.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPatronymic.Name = "labelPatronymic";
            this.labelPatronymic.Size = new System.Drawing.Size(77, 21);
            this.labelPatronymic.TabIndex = 61;
            this.labelPatronymic.Text = "Отчество";
            // 
            // labelAddress
            // 
            this.labelAddress.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddress.Location = new System.Drawing.Point(7, 652);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(53, 21);
            this.labelAddress.TabIndex = 59;
            this.labelAddress.Text = "Адрес";
            // 
            // labelPhone
            // 
            this.labelPhone.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelPhone.AutoSize = true;
            this.labelPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPhone.Location = new System.Drawing.Point(3, 583);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(71, 21);
            this.labelPhone.TabIndex = 58;
            this.labelPhone.Text = "Телефон";
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDateOfBirth.AutoSize = true;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDateOfBirth.Location = new System.Drawing.Point(3, 261);
            this.labelDateOfBirth.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(121, 21);
            this.labelDateOfBirth.TabIndex = 57;
            this.labelDateOfBirth.Text = "Дата рождения";
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxAddress.Location = new System.Drawing.Point(7, 675);
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(189, 145);
            this.textBoxAddress.TabIndex = 11;
            // 
            // labelLogin
            // 
            this.labelLogin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLogin.Location = new System.Drawing.Point(3, 323);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(54, 21);
            this.labelLogin.TabIndex = 52;
            this.labelLogin.Text = "Логин";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 386);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 21);
            this.label2.TabIndex = 51;
            this.label2.Text = "Пароль";
            // 
            // labelPassportSeries
            // 
            this.labelPassportSeries.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelPassportSeries.AutoSize = true;
            this.labelPassportSeries.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassportSeries.Location = new System.Drawing.Point(3, 455);
            this.labelPassportSeries.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPassportSeries.Name = "labelPassportSeries";
            this.labelPassportSeries.Size = new System.Drawing.Size(124, 21);
            this.labelPassportSeries.TabIndex = 50;
            this.labelPassportSeries.Text = "Серия паспорта";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxLogin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLogin.Location = new System.Drawing.Point(7, 347);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(189, 29);
            this.textBoxLogin.TabIndex = 6;
            // 
            // textBoxPatronymic
            // 
            this.textBoxPatronymic.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxPatronymic.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPatronymic.Location = new System.Drawing.Point(7, 222);
            this.textBoxPatronymic.Name = "textBoxPatronymic";
            this.textBoxPatronymic.Size = new System.Drawing.Size(189, 29);
            this.textBoxPatronymic.TabIndex = 4;
            // 
            // labelSurname
            // 
            this.labelSurname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSurname.Location = new System.Drawing.Point(3, 69);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(75, 21);
            this.labelSurname.TabIndex = 44;
            this.labelSurname.Text = "Фамилия";
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblName.Location = new System.Drawing.Point(3, 134);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 21);
            this.lblName.TabIndex = 43;
            this.lblName.Text = "Имя";
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxName.Location = new System.Drawing.Point(7, 158);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(189, 29);
            this.textBoxName.TabIndex = 3;
            this.textBoxName.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxName_Validating);
            this.textBoxName.Validated += new System.EventHandler(this.textBoxName_Validated);
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxSurname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSurname.Location = new System.Drawing.Point(7, 93);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(189, 29);
            this.textBoxSurname.TabIndex = 2;
            this.textBoxSurname.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxSurname_Validating);
            this.textBoxSurname.Validated += new System.EventHandler(this.textBoxSurname_Validated);
            // 
            // dateOfBirthPicker
            // 
            this.dateOfBirthPicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateOfBirthPicker.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateOfBirthPicker.Location = new System.Drawing.Point(7, 285);
            this.dateOfBirthPicker.Name = "dateOfBirthPicker";
            this.dateOfBirthPicker.Size = new System.Drawing.Size(189, 29);
            this.dateOfBirthPicker.TabIndex = 5;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            this.errorProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider.Icon")));
            // 
            // maskedTextBoxPhone
            // 
            this.maskedTextBoxPhone.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.maskedTextBoxPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBoxPhone.Location = new System.Drawing.Point(7, 605);
            this.maskedTextBoxPhone.Mask = "+7 (000) 000-00-00";
            this.maskedTextBoxPhone.Name = "maskedTextBoxPhone";
            this.maskedTextBoxPhone.Size = new System.Drawing.Size(189, 29);
            this.maskedTextBoxPhone.TabIndex = 10;
            this.maskedTextBoxPhone.MouseClick += new System.Windows.Forms.MouseEventHandler(this.maskedTextBoxPhone_MouseClick);
            // 
            // panelAddClient
            // 
            this.panelAddClient.Controls.Add(this.textBoxClientId);
            this.panelAddClient.Controls.Add(this.labelId);
            this.panelAddClient.Controls.Add(this.maskedTextBoxPassword);
            this.panelAddClient.Controls.Add(this.maskedTextBoxPassportId);
            this.panelAddClient.Controls.Add(this.maskedTextBoxPassportSeries);
            this.panelAddClient.Controls.Add(this.textBoxAddress);
            this.panelAddClient.Controls.Add(this.labelAddress);
            this.panelAddClient.Controls.Add(this.maskedTextBoxPhone);
            this.panelAddClient.Controls.Add(this.textBoxName);
            this.panelAddClient.Controls.Add(this.textBoxSurname);
            this.panelAddClient.Controls.Add(this.labelDateOfBirth);
            this.panelAddClient.Controls.Add(this.dateOfBirthPicker);
            this.panelAddClient.Controls.Add(this.labelPassportId);
            this.panelAddClient.Controls.Add(this.textBoxPatronymic);
            this.panelAddClient.Controls.Add(this.labelPatronymic);
            this.panelAddClient.Controls.Add(this.labelPhone);
            this.panelAddClient.Controls.Add(this.labelSurname);
            this.panelAddClient.Controls.Add(this.labelLogin);
            this.panelAddClient.Controls.Add(this.lblName);
            this.panelAddClient.Controls.Add(this.label2);
            this.panelAddClient.Controls.Add(this.labelPassportSeries);
            this.panelAddClient.Controls.Add(this.textBoxLogin);
            this.panelAddClient.Location = new System.Drawing.Point(3, -2);
            this.panelAddClient.Name = "panelAddClient";
            this.panelAddClient.Size = new System.Drawing.Size(211, 883);
            this.panelAddClient.TabIndex = 68;
            // 
            // textBoxClientId
            // 
            this.textBoxClientId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxClientId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxClientId.Location = new System.Drawing.Point(7, 29);
            this.textBoxClientId.Name = "textBoxClientId";
            this.textBoxClientId.Size = new System.Drawing.Size(189, 29);
            this.textBoxClientId.TabIndex = 0;
            this.textBoxClientId.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxClientId_Validating);
            this.textBoxClientId.Validated += new System.EventHandler(this.textBoxClientId_Validated);
            // 
            // labelId
            // 
            this.labelId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelId.AutoSize = true;
            this.labelId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelId.Location = new System.Drawing.Point(3, 5);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(119, 21);
            this.labelId.TabIndex = 64;
            this.labelId.Text = "Номер клиента";
            // 
            // maskedTextBoxPassword
            // 
            this.maskedTextBoxPassword.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.maskedTextBoxPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBoxPassword.Location = new System.Drawing.Point(7, 410);
            this.maskedTextBoxPassword.Mask = "aaaaaa";
            this.maskedTextBoxPassword.Name = "maskedTextBoxPassword";
            this.maskedTextBoxPassword.Size = new System.Drawing.Size(189, 29);
            this.maskedTextBoxPassword.TabIndex = 7;
            this.maskedTextBoxPassword.MouseClick += new System.Windows.Forms.MouseEventHandler(this.maskedTextBoxPassword_MouseClick);
            // 
            // maskedTextBoxPassportId
            // 
            this.maskedTextBoxPassportId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.maskedTextBoxPassportId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBoxPassportId.Location = new System.Drawing.Point(7, 543);
            this.maskedTextBoxPassportId.Mask = "000000";
            this.maskedTextBoxPassportId.Name = "maskedTextBoxPassportId";
            this.maskedTextBoxPassportId.Size = new System.Drawing.Size(189, 29);
            this.maskedTextBoxPassportId.TabIndex = 9;
            this.maskedTextBoxPassportId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.maskedTextBoxPassportId_MouseClick);
            // 
            // maskedTextBoxPassportSeries
            // 
            this.maskedTextBoxPassportSeries.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.maskedTextBoxPassportSeries.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBoxPassportSeries.Location = new System.Drawing.Point(7, 478);
            this.maskedTextBoxPassportSeries.Mask = "0000";
            this.maskedTextBoxPassportSeries.Name = "maskedTextBoxPassportSeries";
            this.maskedTextBoxPassportSeries.Size = new System.Drawing.Size(189, 29);
            this.maskedTextBoxPassportSeries.TabIndex = 8;
            this.maskedTextBoxPassportSeries.MouseClick += new System.Windows.Forms.MouseEventHandler(this.maskedTextBoxPassportSeries_MouseClick);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer.Location = new System.Drawing.Point(0, -1);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.AutoScroll = true;
            this.splitContainer.Panel1.Controls.Add(this.panelAddClient);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.pictureBoxLogo);
            this.splitContainer.Panel2.Controls.Add(this.groupBoxButtons);
            this.splitContainer.Size = new System.Drawing.Size(712, 455);
            this.splitContainer.SplitterDistance = 237;
            this.splitContainer.TabIndex = 69;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(-4, -2);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(481, 390);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 67;
            this.pictureBoxLogo.TabStop = false;
            // 
            // groupBoxButtons
            // 
            this.groupBoxButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxButtons.Controls.Add(this.btnCancel);
            this.groupBoxButtons.Controls.Add(this.btnAdd);
            this.groupBoxButtons.Location = new System.Drawing.Point(2, 384);
            this.groupBoxButtons.Name = "groupBoxButtons";
            this.groupBoxButtons.Size = new System.Drawing.Size(467, 67);
            this.groupBoxButtons.TabIndex = 66;
            this.groupBoxButtons.TabStop = false;
            // 
            // AddClientForm
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(712, 454);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить клиента";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.panelAddClient.ResumeLayout(false);
            this.panelAddClient.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.groupBoxButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label labelPassportId;
        private System.Windows.Forms.Label labelPatronymic;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPassportSeries;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxPatronymic;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.DateTimePicker dateOfBirthPicker;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPhone;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panelAddClient;
        private System.Windows.Forms.GroupBox groupBoxButtons;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPassword;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPassportId;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPassportSeries;
        private System.Windows.Forms.TextBox textBoxClientId;
        private System.Windows.Forms.Label labelId;
    }
}