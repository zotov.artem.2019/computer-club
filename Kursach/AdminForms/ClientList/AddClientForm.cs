﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Kursach.AdminForms
{
    public partial class AddClientForm : Form
    {
        private short clientId;
        private string name;
        private string surname;
        private string patronymic;
        private string login;
        private string password;
        private string passportSeries;
        private string passportId;
        private string address;
        private string phone;
        private DateTime dateOfBirth;
        private EntityManager manager = new EntityManager();
        public AddClientForm()
        {
            InitializeComponent();
        }
             

        private void textBoxSurname_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxSurname.Text.Trim();
            if (String.IsNullOrEmpty(input))
            {
                errorProvider.SetError(textBoxSurname, "Ошибка!");
                e.Cancel = true;
            }
            else
            {
                errorProvider.SetError(textBoxSurname, String.Empty);
                e.Cancel = false;
            }

        }

        private void textBoxName_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxName.Text.Trim();
            if (String.IsNullOrEmpty(input))
            {
                errorProvider.SetError(textBoxName, "Ошибка!");
                e.Cancel = true;
            }
            else
            {
                errorProvider.SetError(textBoxName, String.Empty);
                e.Cancel = false;
            }

        }

        private void textBoxSurname_Validated(object sender, EventArgs e)
        {
            surname = textBoxSurname.Text.Trim();
        }

        private void textBoxName_Validated(object sender, EventArgs e)
        {
            name = textBoxName.Text.Trim();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddClient();
            this.Close();
        }

        private void AddClient()
        {    
            patronymic = textBoxPatronymic.Text.Trim();
            dateOfBirth = dateOfBirthPicker.Value;
            address = textBoxAddress.Text.Trim();
            login = textBoxLogin.Text.Trim();
            password = maskedTextBoxPassword.Text.Trim();
            passportId = maskedTextBoxPassportId.Text.Trim();
            passportSeries = maskedTextBoxPassportSeries.Text.Trim();
            phone = maskedTextBoxPhone.Text.Trim();
            try
            {
                using (var db = new ComputerClubModel())
                {
                   db.Clients.Add(new Client
                    {
                        Client_ID = clientId,
                        Surname = surname,
                        Name = name,
                        Patronymic = patronymic,
                        Login = login,
                        Password = password,
                        Passport_Series = passportSeries,
                        Passport_ID = passportId,
                        Address = address,
                        Phone = phone,
                        Date_of_birth = dateOfBirth
                    });                   
                    db.SaveChanges();
                }
                MessageBox.Show("Данные добавлены!", "Добавлено", MessageBoxButtons.OK);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка в данных!", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void maskedTextBoxPassword_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassword.Select(maskedTextBoxPassword.Text.Length, 0);
        }

        private void maskedTextBoxPassportSeries_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassportSeries.Select(maskedTextBoxPassportSeries.Text.Length, 0);
        }

        private void maskedTextBoxPassportId_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPassportId.Select(maskedTextBoxPassportId.Text.Length, 0);
        }

        private void textBoxClientId_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxClientId.Text.Trim();
            using (var db = new ComputerClubModel())
            {
                
                if (db.Clients.Where(cl => cl.Client_ID.ToString() == input).Any())
                {
                    errorProvider.SetError(textBoxClientId, "Клиент с таким номером уже существует!");
                    e.Cancel = true;
                } 
                else if (Regex.IsMatch(input, @"(?<=\s|^)\d+(?=\s|$)"))
                {
                    errorProvider.SetError(textBoxClientId, String.Empty);
                    e.Cancel = false;
                }
                else
                {
                    errorProvider.SetError(textBoxClientId, "Ошибка!");
                    e.Cancel = true;
                }
            }
        }

        private void textBoxClientId_Validated(object sender, EventArgs e)
        {
            clientId = (short)Convert.ToInt32(textBoxClientId.Text.Trim());
        }

        private void maskedTextBoxPhone_MouseClick(object sender, MouseEventArgs e)
        {
            maskedTextBoxPhone.SelectionStart = 0;
        }
    }
}
