﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach
{
    public partial class UpdateClientForm : Form
    {
        private EntityManager manager = new EntityManager(); 
        private short clientId;
        private string name;
        private string surname;
        private string patronymic;
        private string login;
        private string password;
        private string passportSeries;
        private string passportId;
        private string address;
        private string phone;
        private DateTime dateOfBirth;
        public UpdateClientForm(short clientId)
        {
            InitializeComponent();
            this.clientId = clientId;
        }

        private void UpdateClient()
        {
            try
            {
                patronymic = textBoxPatronymic.Text.Trim();
                login = textBoxLogin.Text.Trim();
                password = maskedTextBoxPassword.Text.Trim();
                passportSeries = maskedTextBoxPassportSeries.Text.Trim();
                passportId = maskedTextBoxPassportId.Text.Trim();
                address = textBoxAddress.Text.Trim();
                dateOfBirth = dateOfBirthPicker.Value;
                phone = maskedTextBoxPhone.Text.Trim();
                using (var db = new ComputerClubModel())
                {
                    Client client = db.Clients.Where(x => x.Client_ID == clientId).First();
                    client.Surname = surname;
                    client.Name = name;
                    client.Patronymic = patronymic;
                    client.Login = login;
                    client.Password = password;
                    client.Passport_Series = passportSeries;
                    client.Passport_ID = passportId;
                    client.Address = address;
                    client.Phone = phone;
                    client.Date_of_birth = dateOfBirth;
                    db.SaveChanges();
                }
            } catch (Exception ex)
            {
                MessageBox.Show("Некорректные ФИО");
            }
            
        }

       
        private void textBoxSurname_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxSurname.Text.Trim();
            if (String.IsNullOrEmpty(input))
            {
                errorProvider.SetError(textBoxSurname, "Ошибка!");
                e.Cancel = true;
            }
            else
            {
                errorProvider.SetError(textBoxSurname, String.Empty);
                e.Cancel = false;
            }

        }

        private void textBoxName_Validating(object sender, CancelEventArgs e)
        {
            string input = textBoxName.Text.Trim();
            if (String.IsNullOrEmpty(input))
            {
                errorProvider.SetError(textBoxName, "Ошибка!");
                e.Cancel = true;
            }
            else
            {
                errorProvider.SetError(textBoxName, String.Empty);
                e.Cancel = false;
            }

        }

        private void textBoxSurname_Validated(object sender, EventArgs e)
        {
            surname = textBoxSurname.Text.Trim();
        }

        private void textBoxName_Validated(object sender, EventArgs e)
        {
            name = textBoxName.Text.Trim();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите изменить этот элемент?",
                "Изменение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            DialogResult = ValidateChildren() ? DialogResult.OK : DialogResult.None;
            if (DialogResult == DialogResult.OK)
            {
                UpdateClient();
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            Close();
        }

        private void UpdateClientForm_Load(object sender, EventArgs e)
        {
            using (var db = new ComputerClubModel())
            {
                Client client = db.Clients.Where(c => c.Client_ID == clientId).First();
                textBoxClientId.Text = clientId.ToString();
                textBoxLogin.Text = client.Login;
                maskedTextBoxPassword.Text = client.Password;
                maskedTextBoxPhone.Text = client.Phone;
                maskedTextBoxPassportSeries.Text = client.Passport_Series.ToString();
                maskedTextBoxPassportId.Text = client.Passport_ID.ToString();
                textBoxAddress.Text = client.Address;
                textBoxSurname.Text = client.Surname;
                textBoxName.Text = client.Name;
                textBoxPatronymic.Text = client.Patronymic;
                dateOfBirthPicker.Value = client.Date_of_birth;
            }
               
        }
    }
}
