﻿namespace Kursach
{
    partial class ClientListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientListForm));
            this.ClientsGridView = new System.Windows.Forms.DataGridView();
            this.ClientContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addClientMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBack = new System.Windows.Forms.Button();
            this.splitContainerClient = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.ClientsGridView)).BeginInit();
            this.ClientContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerClient)).BeginInit();
            this.splitContainerClient.Panel1.SuspendLayout();
            this.splitContainerClient.Panel2.SuspendLayout();
            this.splitContainerClient.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClientsGridView
            // 
            this.ClientsGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ClientsGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ClientsGridView.ContextMenuStrip = this.ClientContextMenu;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ClientsGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.ClientsGridView.Location = new System.Drawing.Point(-5, -2);
            this.ClientsGridView.Name = "ClientsGridView";
            this.ClientsGridView.Size = new System.Drawing.Size(939, 636);
            this.ClientsGridView.TabIndex = 0;
            // 
            // ClientContextMenu
            // 
            this.ClientContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addClientMenuItem,
            this.updateMenuItem,
            this.deleteMenuItem,
            this.refreshMenuItem});
            this.ClientContextMenu.Name = "ClientContextMenu";
            this.ClientContextMenu.Size = new System.Drawing.Size(129, 92);
            // 
            // addClientMenuItem
            // 
            this.addClientMenuItem.Name = "addClientMenuItem";
            this.addClientMenuItem.Size = new System.Drawing.Size(128, 22);
            this.addClientMenuItem.Text = "Добавить";
            this.addClientMenuItem.Click += new System.EventHandler(this.addClientMenuItem_Click);
            // 
            // updateMenuItem
            // 
            this.updateMenuItem.Name = "updateMenuItem";
            this.updateMenuItem.Size = new System.Drawing.Size(128, 22);
            this.updateMenuItem.Text = "Изменить";
            this.updateMenuItem.Click += new System.EventHandler(this.updateMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(128, 22);
            this.deleteMenuItem.Text = "Удалить";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // refreshMenuItem
            // 
            this.refreshMenuItem.Name = "refreshMenuItem";
            this.refreshMenuItem.Size = new System.Drawing.Size(128, 22);
            this.refreshMenuItem.Text = "Обновить";
            this.refreshMenuItem.Click += new System.EventHandler(this.refreshMenuItem_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBack.Location = new System.Drawing.Point(2, 2);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(146, 47);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Назад";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // splitContainerClient
            // 
            this.splitContainerClient.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainerClient.Location = new System.Drawing.Point(0, 0);
            this.splitContainerClient.Name = "splitContainerClient";
            // 
            // splitContainerClient.Panel1
            // 
            this.splitContainerClient.Panel1.Controls.Add(this.btnBack);
            // 
            // splitContainerClient.Panel2
            // 
            this.splitContainerClient.Panel2.Controls.Add(this.ClientsGridView);
            this.splitContainerClient.Size = new System.Drawing.Size(1094, 636);
            this.splitContainerClient.SplitterDistance = 154;
            this.splitContainerClient.TabIndex = 41;
            // 
            // ClientListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnBack;
            this.ClientSize = new System.Drawing.Size(1092, 636);
            this.Controls.Add(this.splitContainerClient);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ClientListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список клиентов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientListForm_FormClosing);
            this.Load += new System.EventHandler(this.ClientListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ClientsGridView)).EndInit();
            this.ClientContextMenu.ResumeLayout(false);
            this.splitContainerClient.Panel1.ResumeLayout(false);
            this.splitContainerClient.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerClient)).EndInit();
            this.splitContainerClient.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ClientsGridView;
        private System.Windows.Forms.ContextMenuStrip ClientContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addClientMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshMenuItem;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.SplitContainer splitContainerClient;
    }
}