﻿using Kursach.AdminForms.CardList;
using Kursach.AdminForms.ComputerList;
using Kursach.AdminForms.Employees;
using Kursach.AdminForms.Rents;
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Kursach
{
    public partial class AdminForm : Form
    {
        private Employee admin;
        private EntityManager manager = new EntityManager();
        private int currentGirdIndex;
        private DataGridView currentGrid;
        public AdminForm(Employee admin)
        {
            InitializeComponent();
            this.admin = admin;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            AuthorizationForm authorizationForm = new AuthorizationForm();
            this.Hide();
            authorizationForm.ShowDialog();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            GetCurrentGrid();
            userName.Text ="ФИО: " + admin.Surname + " " + admin.Name + " " + admin.Patronymic;
            labelEmployeeId.Text ="Номер сотрудника: " + admin.Employee_ID.ToString();
            labelStazh.Text = "Стаж: " + Math.Floor((DateTime.Now - admin.Hire_date).TotalDays/12).ToString() + " месяцев";
            labelYearsOld.Text = "Возраст " + Math.Floor((DateTime.Now - admin.Date_of_birth).TotalDays/365).ToString();
            SetEmployeeGrid(dataGridViewPCMasters, manager.GetAllEmployeesByPosition(2));
            SetEmployeeGrid(dataGridViewCleaners, manager.GetAllEmployeesByPosition(3));
            SetEmployeeGrid(dataGridViewSecurity, manager.GetAllEmployeesByPosition(4));

        }

        private void GetCurrentGrid()
        {
            DataGridView grid = new DataGridView();
            if (currentGirdIndex == 0)
            {
                currentGrid = dataGridViewPCMasters;
            } else if (currentGirdIndex == 1)
            {
                currentGrid = dataGridViewCleaners;
            } else
            {
                currentGrid = dataGridViewSecurity;
            }
        }

        private void AdminForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void buttonClientList_Click(object sender, EventArgs e)
        {
            ClientListForm clientListForm = new ClientListForm(admin);
            this.Hide();
            clientListForm.ShowDialog();
        }

        private void buttonClubCards_Click(object sender, EventArgs e)
        {
            ClubCardForm cardForm = new ClubCardForm(admin);
            this.Hide();
            cardForm.ShowDialog();
        }

        private void buttonCurrentRents_Click(object sender, EventArgs e)
        {
            CurrentRentsForm currentRentsForm = new CurrentRentsForm(admin);
            this.Hide();
            currentRentsForm.ShowDialog();
        }

        private void buttonComputerList_Click(object sender, EventArgs e)
        {
            ComputerListForm computerListForm = new ComputerListForm(admin);
            this.Hide();
            computerListForm.ShowDialog();
        }
        private void SetEmployeeGrid(DataGridView dataGrid,IEnumerable employees)
        {
            dataGrid.DataSource = employees;
            dataGrid.Columns[0].HeaderText = "Номер сотрудника";
            dataGrid.Columns[1].HeaderText = "Фамилия";
            dataGrid.Columns[2].HeaderText = "Имя";
            dataGrid.Columns[3].HeaderText = "Отчество";
            dataGrid.Columns[4].HeaderText = "Дата рождения";
            dataGrid.Columns[5].HeaderText = "Серия паспорта";
            dataGrid.Columns[6].HeaderText = "Номер паспорта";
            dataGrid.Columns[7].HeaderText = "Адрес";
            dataGrid.Columns[8].HeaderText = "Телефон";
            dataGrid.Columns[9].HeaderText = "Дата найма";
        }

        private void addEmployeeMenuItem_Click(object sender, EventArgs e)
        {
            AddEmployeeForm add = new AddEmployeeForm();
            if (add.ShowDialog(this) == DialogResult.OK)
            {
                SetEmployeeGrid(dataGridViewPCMasters, manager.GetAllEmployeesByPosition(2));
                SetEmployeeGrid(dataGridViewCleaners, manager.GetAllEmployeesByPosition(3));
                SetEmployeeGrid(dataGridViewSecurity, manager.GetAllEmployeesByPosition(4));
            }
        }

        private void updateMenuItem_Click(object sender, EventArgs e)
        {
            if (currentGrid.SelectedCells.Count > 0)
            {
                var i = currentGrid.SelectedCells[0].OwningRow.Index;
                short employeeId = (short)currentGrid[0, i].Value;
                UpdateEmployeeForm update = new UpdateEmployeeForm(employeeId);
                if (update.ShowDialog(this) == DialogResult.OK)
                {
                    SetEmployeeGrid(dataGridViewPCMasters, manager.GetAllEmployeesByPosition(2));
                    SetEmployeeGrid(dataGridViewCleaners, manager.GetAllEmployeesByPosition(3));
                    SetEmployeeGrid(dataGridViewSecurity, manager.GetAllEmployeesByPosition(4));
                }
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            
            DialogResult result = System.Windows.Forms.MessageBox.Show("Вы уверены, что хотите удалить этот элемент?",
                "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                if (currentGrid.SelectedCells.Count > 0)
                {
                    var i = currentGrid.SelectedCells[0].OwningRow.Index;
                    short emplId = (short)currentGrid[0, i].Value;
                    using (var db = new ComputerClubModel())
                    {
                        Employee employee = db.Employees.Where(empl => empl.Employee_ID == emplId).First();
                        db.Employees.Remove(employee);
                        db.SaveChanges();
                    }
                }
            }
            SetEmployeeGrid(dataGridViewPCMasters, manager.GetAllEmployeesByPosition(2));
            SetEmployeeGrid(dataGridViewCleaners, manager.GetAllEmployeesByPosition(3));
            SetEmployeeGrid(dataGridViewSecurity, manager.GetAllEmployeesByPosition(4));
        }

        private void refreshMenuItem_Click(object sender, EventArgs e)
        {
            SetEmployeeGrid(dataGridViewPCMasters, manager.GetAllEmployeesByPosition(2));
            SetEmployeeGrid(dataGridViewCleaners, manager.GetAllEmployeesByPosition(3));
            SetEmployeeGrid(dataGridViewSecurity, manager.GetAllEmployeesByPosition(4));
        }

        private void tabControlEmployees_Selecting(object sender, TabControlCancelEventArgs e)
        {
            currentGirdIndex = tabControlEmployees.SelectedIndex;
            GetCurrentGrid();
        }
    }
}
